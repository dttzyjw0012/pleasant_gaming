<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/5 0005
  Time: 下午 3:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>舒游个人资料</title>
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap/css/bootstrap.min.css"/>
    <style type="text/css">
        #first {
            width: 1076.2px;
            margin: auto;
        }

        .navbar-inverse {
            background-color: white;
            border-color: #080808;
            margin: auto;
        }

        .navbar {
            position: relative;
            min-height: 50px;
            margin-bottom: 20px;
            border: 1px;
        }

        .navbar-inverse .navbar-nav > li > a {
            color: black;
            font-size: 12px;
        }

        .navbar-inverse .navbar-brand {
            color: black;
        }

        .navbar-inverse .navbar-brand:hover {
            color: orangered;
        }

        .navbar-inverse .navbar-nav > li > a:hover {
            color: orangered;
        }

        #LoginInfo {
            width: 210px;
            height: 195px;
            margin-bottom: 10px;
            float: left;
            border: 2px #c4d5e0 solid;
        }

        #LoginInfo p {
            line-height: 28px;
            text-indent: 4px;
            font-size: 12px;
            color: #333;
            width: 1050px;
            margin: 0 auto;
            word-wrap: break-word;
            word-break: break-all;
            height: 27px;
            overflow: hidden;
        }

        #rightBlock {
            width: 835px;
            height: 210px;
            float: left;
            border: 2px #c4d5e0 solid;
            margin-left: 5px;
        }

        .brief {
            width: 708px;
            height: auto;
            border: 1px;
            padding-left: 20px;
            padding-bottom: 5px;
        }

        .brief .rightIn {
            width: 445px;
            font-size: 12px;
            float: left;
            height: auto;
            margin-top: 15px;
            color: #999;
            line-height: 24px;
        }

        .brief .rightIn .user {
            font-size: 14px;
            padding-right: 10px;
            color: #666;
        }

        .brief .rightIn .idnumber {
            color: #266392;
            margin-right: 10px;
        }

        .approve {
            color: #999;
            font-size: 12px;
            margin-left: 10px;
        }

        .brief .rightIn p.last {
            padding-top: 4px;
        }

        .brief .rightIn p.safe {
            padding-top: 4px;
            margin-bottom: 10px;
            padding-bottom: 5px;
        }

        .tips {
            color: #999;
        }

        .brief-info .alert-w a {
            text-decoration: underline;
        }

        .brief .hint-right {
            float: right;
            height: 30px;
            margin-top: 20px;
            margin-right: 20px;
            width: 154px;
        }

        .brief-info .bar-k a {
            display: inline-block;
            height: 25px;
            width: 71px;
            float: left;
            margin-right: 10px;
        }

        #middleBox {
            display: block;
            width: 1076.2px;
            margin: auto;
            height: 210px;
            margin-bottom: 10px;
        }

        .middleDown {
            margin: auto;
            width: 1050px;
            height: 550px;
        }

        .left-down {
            width: 96px;
            height: 350px;
            border: 1px #c4d5e0 solid;
            margin-top: 3px;
            margin-left: 3px;
            float: left;
        }

        .account-info {
            width: 730px;
            height: 165px;
            margin-top: 10px;
            float: left;
            margin-left: 170px;
            border: 1px #c4d5e0 solid;
        }

        .c2 {
            color: #999;
        }

        #service {
            width: 360px;
            height: 300px;
            margin-top: 10px;
            border: 1px solid #c4d5e0;
            float: left;
            margin-left: 170px;
        }

        .service-left {
            color: orangered;
        }

        #remind {
            width: 360px;
            height: 300px;
            margin-top: 10px;
            margin-left: 20px;
            border: 1px solid #c4d5e0;
            float: left;
        }

        .remind-left {
            color: orangered;
        }

        #footer {
            width: 1000px;
            height: 176px;
            margin: auto;
        }

        #foot {
            width: 600px;
            height: 20px;
            margin: auto;
            margin-top: 30px;
        }

        #foot li {
            width: 66.4px;
            height: 18px;
            color: #DDDDDD;
            list-style: none;
            float: left;
            font-size: 12px;
        }

        #foot a {
            width: 48px;
            height: 14.4px;
            margin-right: 8px;
            color: #999999;
        }

        #foot-1 {
            width: 1000px;
            height: 48px;
            padding-top: 15px;
            padding-bottom: 15px;
            margin-left: 60px;
        }

        #foot-1 a {
            margin-left: 10px;
            color: #999999;
            font-size: 12px;
        }

        #foot-2-1 {
            width: 1000px;
            height: 36px;
        }

        #foot-2-1 li {
            float: left;
            color: #DDDDDD;
            list-style: none;
            font-size: 12px;
        }

        #foot-2-1 a {
            margin-left: 8px;
            margin-right: 8px;
            color: #999999;
        }

        .navbar-inverse .navbar-nav > li > a:focus, .navbar-inverse .navbar-nav > li > a:hover {
            color: red;
            background-color: transparent;
        }
    </style>
</head>

<body>
<%--<%--%>
<%--List<GameType> gameTypelist = (List<GameType>) request.getAttribute("gameTypelist");--%>
<%--//            List<Game> gamelist = (List<Game>) request.getAttribute("gameList");--%>
<%--GetTypeById getTypeById = (GetTypeById) request.getAttribute("getTypeById");--%>

<%--int index = 0;--%>
<%--%>--%>
<div class="container-fluid">
    <div class="row">
        <div id="first">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#example-navbar-collapse">
                            <span class="sr-only">切换导航</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="..//"><img src="img/logo_140.png"/></a>
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-collapse">
                        <ul class="nav navbar-nav navbar-right mya">
                            <li>
                                <a href="javascript:void(0);">您好，尊敬的用户</a>
                            </li>
                            <li>
                                <a href="pro_info.jsp">我的舒游</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">我的收藏</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">我要买</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">我要卖</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">客服中心</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">帮助中心</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>


        <div id="middleBox">
            <div id="LoginInfo" style="display: block;">
                <div class="L-c">
                    <p>
                        <span class="u-Icon">&nbsp;登录用户信息:</span>
                    </p>

                    <div id="_userInfo" style="display: block">
                        <p>买家成交数：<span>0</span></p>

                        <p>卖家成交数：<span>0</span></p>
                        <p>
                            芝麻信用：
                            <span>
				         			<a href="javascript:void(0);">暂无分值</a>
				              	    </span>
                            <img src="img/ico_zmxy.png"/>

                        </p>
                        <p>
                            我的金钻：
                            <span>
				                        <a href="javascript:void(0);" style=" color:#f60 !important;">
				                            0.00
				                        </a>
				                    </span>
                            <img src="img/diamon_logo.png"/>
                        </p>
                        <p style="  margin-top:3px;">
                            <a href="javascript:void(0);">进入我的账户</a>
                        </p>

                        <p style="  margin-top:3px;">
                            [
                            <a href="pro_info.jsp">我的舒游</a>]&nbsp;&nbsp; [
                            <a href="javascript:void(0);">退出登录</a>]

                        </p>
                    </div>
                </div>
            </div>

            <div id="rightBlock" style="display: block;">
                <div class="brief">
                    <div class="rightIn">
                        <p>
									<span class="user">
				        					晚上好，
				        					<span class="idnumber">5201314</span> (
									<a class="more_name" href="javascript:void(0);">点此完善昵称</a>)
									<span class="approve">(未认证)</span>
									</span>
                        </p>
                        <p>
                            您当前的登陆IP：
                            <%=request.getRemoteAddr()%>
                        </p>
                        <p class="last">
                            您上次登陆的时间：2018-09-02 21:52(不是您登陆的?
                            <a href="javascript:void(0);">请点这里</a>)
                        </p>
                        <p class="safe">
                            您账号安全等级：
                            <img src="img/safety_level_2.png"/>
                            <a class="tips" href="javascript:void(0);">提高安全等级</a>
                        </p>
                    </div>
                    <div class="hint-right">
                        <button type="button" class="btn btn-warning">我要出售</button>
                        <button type="button" class="btn btn-success">我要购买</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="middleDown">
            <!--按钮下拉菜单-->
            <div class="left-down">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        我是买家 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">我要卖</a></li>
                        <li><a href="javascript:void(0);">我购买的商品</a></li>
                        <li><a href="javascript:void(0);">商城订单</a></li>
                        <li><a href="javascript:void(0);">还价订单</a></li>
                        <li><a href="javascript:void(0);">租赁订单</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        我是卖家 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">我要卖</a></li>
                        <li><a href="javascript:void(0);">出售中的商品</a></li>
                        <li><a href="javascript:void(0);">商城订单</a></li>
                        <li><a href="javascript:void(0);">还价订单</a></li>
                        <li><a href="javascript:void(0);">最新成交价格</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        收货中心 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">我要收货</a></li>
                        <li><a href="javascript:void(0);">收货订单</a></li>
                        <li><a href="javascript:void(0);">出货订单</a></li>
                        <li><a href="javascript:void(0);">分仓订单</a></li>
                        <li><a href="javascript:void(0);">库存明细</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        资金管理 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">我的账户</a></li>
                        <li><a href="javascript:void(0);">我要提现</a></li>
                        <li><a href="javascript:void(0);">资金明细</a></li>
                        <li><a href="javascript:void(0);">提现明细</a></li>
                        <li><a href="javascript:void(0);">押金取回</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        账号设置 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">完善个人信息</a></li>
                        <li><a href="javascript:void(0);">个人信用度</a></li>
                        <li><a href="javascript:void(0);">收货角色管理</a></li>
                        <li><a href="javascript:void(0);">发布账号管理</a></li>
                        <li><a href="javascript:void(0);">密保卡管理</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        消息中心 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">我的站内信</a></li>
                        <li><a href="javascript:void(0);">站内信接受设置</a></li>
                        <li><a href="javascript:void(0);">我的短信账单</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        投诉咨询 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">我要投诉</a></li>
                        <li><a href="javascript:void(0);">我要咨询</a></li>
                        <li><a href="javascript:void(0);">修改邮箱申请</a></li>
                        <li><a href="javascript:void(0);">资金异常核对申请</a></li>
                        <li><a href="javascript:void(0);">解封账号申请</a></li>
                        <li><a href="javascript:void(0);">修改实名信息申请</a></li>
                        <li><a href="javascript:void(0);">解除手机密保申请</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        舒游积分 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">积分换购</a></li>
                        <li><a href="javascript:void(0);">兑换明细</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        我的店铺 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">分类管理</a></li>
                        <li><a href="javascript:void(0);">宝贝管理</a></li>
                        <li><a href="javascript:void(0);">广告商品</a></li>
                        <li><a href="javascript:void(0);">设置客服</a></li>
                        <li><a href="javascript:void(0);">店铺信息</a></li>
                        <li><a href="javascript:void(0);">收藏</a></li>
                        <li><a href="javascript:void(0);">认证店铺</a></li>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        推广联盟 <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);">我的推广链接</a></li>
                        <li><a href="javascript:void(0);">分成明细</a></li>
                        <li><a href="javascript:void(0);">分成转账</a></li>
                    </ul>
                </div>
            </div>
            <!--安全中心-->
            <div class="account-info">
                <table class="table1">

                    <colgroup>
                        <col class="td1">
                        <col class="td2">
                        <col class="td3">
                        <col class="td4">
                    </colgroup>
                    <tbody>
                    <tr class="title">
                        <td class="td1"><b style="color: orangered;">我的安全中心</b></td>
                        <td class="td5" colspan="2"></td>
                    </tr>

                    <tr>

                        <td class="c1 set">手机绑定</td>
                        <td class="c2">&nbsp;&nbsp;&nbsp;&nbsp;绑定手机用来接收支付、发布等操作的短信验证码，提高资金及交易的安全性。</td>
                        <td>
                            <a target="_blank" href="javascript:void(0);">&nbsp;&nbsp;&nbsp;&nbsp;换绑</a>
                        </td>

                    </tr>

                    <tr>
                        <td class="c1 td1 set">登录密码</td>
                        <td class="c2 ">&nbsp;&nbsp;&nbsp;&nbsp;英文加数字或符号的组合密码，安全性更高，建议您定期更换密码。</td>

                        <td>
                            <a href="javascript:void(0);">&nbsp;&nbsp;&nbsp;&nbsp;修改</a>
                        </td>
                    </tr>
                    <tr>

                        <td class="c1 td1 set">实名信息</td>
                        <td class="c2 ">&nbsp;&nbsp;&nbsp;&nbsp;发布、购买商品时需完成实名认证，使您的交易更安全。</td>
                        <td>
                            <a href="javascript:void(0);">&nbsp;&nbsp;&nbsp;&nbsp;修改</a>
                        </td>

                    </tr>
                    <tr>

                        <td class="c1 td1 unset">支付密码</td>
                        <td class="c2 ">&nbsp;&nbsp;&nbsp;&nbsp;在您支付时，输入正确的支付密码，保障您的资金和账户更安全。</td>
                        <td>
                            <a target="_blank" href="javascript:void(0);">&nbsp;&nbsp;&nbsp;&nbsp;设置</a>
                        </td>

                    </tr>
                    <tr>

                        <td class="c1 td1 set">手机动态口令</td>
                        <td class="c2 ">&nbsp;&nbsp;&nbsp;&nbsp;为您的账号做更安全的保障。</td>
                        <td>
                            <a target="_blank" href="javascript:void(0);">&nbsp;&nbsp;&nbsp;&nbsp;查看</a>
                        </td>

                    </tr>
                    <tr>

                        <td class="c1 td1 set">密码提示问题</td>
                        <td class="c2 ">&nbsp;&nbsp;&nbsp;&nbsp;用于找回密码、修改个人信息等，使您的信息更安全。</td>
                        <td>
                            <a target="_blank" href="javascript:void(0);">&nbsp;&nbsp;&nbsp;&nbsp;修改</a>
                        </td>

                    </tr>

                    </tbody>
                </table>

            </div>
            <!--便民中心-->
            <div id="service">
                <div class="service-left"><b>便民中心</b></div>
                <hr>
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#home" data-toggle="tab">
                            游戏币
                        </a>
                    </li>
                    <li><a href="#QQ" data-toggle="tab">QQ</a></li>
                    <li><a href="#dianka" data-toggle="tab">点卡</a></li>


                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="home">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="firstname" class="col-sm-2 control-label">名称</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstname"
                                           placeholder="请输入游戏名称">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastname" class="col-sm-2 control-label">区服</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastname"
                                           placeholder="请输入游戏区服">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nextname" class="col-sm-2 control-label">金额</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nextname"
                                           placeholder="请输入购买金额">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">点击购买</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="QQ">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="qqNum" class="col-sm-2 control-label">账号</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="qqNum"
                                           placeholder="请输入您的QQ号码">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="rechargeAmountMoney" class="col-sm-2 control-label">面值</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="rechargeAmountMoney"
                                           placeholder="请输入充值面值数">
                                </div>
                            </div>
                            <p><b>&nbsp;&nbsp;类型</b>：&nbsp;&nbsp;&nbsp;Q币充值</p>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">立即充值</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="dianka">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="firstnameDianka" class="col-sm-2 control-label">名称</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstnameDianka"
                                           placeholder="请输入游戏名称">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastnameDianka" class="col-sm-2 control-label">点券</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastnameDianka"
                                           placeholder="请输入充值点券数">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">立即充值</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--安全交易提醒-->
            <div id="remind">
                <div class="remind-left"><b>安全交易提醒</b></div>
                <hr>
                <ul>

                    <li><a href="javascript:void(0);" target="_blank">钓鱼网站案例分析</a></li>

                    <li><a href="javascript:void(0);" target="_blank">购买低价商品被钓鱼后资金被盗</a></li>

                    <li><a href="javascript:void(0);" target="_blank">低价游戏币换来高价“担保金”</a></li>

                    <li><a href="javascript:void(0);" target="_blank">假客服以充积分名义骗取用户资金</a></li>

                    <li><a href="javascript:void(0);" target="_blank">邮件假链接案例分析</a></li>

                </ul>
                <img src="img/safe_ico1.png"/>
                <button type="button" class="btn btn-default">真假客服验证</button>&nbsp;&nbsp;&nbsp;&nbsp;
                <img src="img/safe_ico2.png"/>
                <button type="button" class="btn btn-default">真假银行账户验证</button>&nbsp;&nbsp;
                <img src="img/safe_ico3.png"/>
                <button type="button" class="btn btn-default">真假网址验证</button>&nbsp;&nbsp;&nbsp;&nbsp;
                <img src="img/safe_ico4.png"/>
                <button type="button" class="btn btn-default">邮箱地址验证</button>&nbsp;&nbsp;

            </div>
        </div>
    </div>
</div>

<div id="footer">
    <ul id="foot">
        <li><a href="javascript:void(0);">关于我们</a>|</li>
        <li><a href="javascript:void(0);">合作伙伴</a>|</li>
        <li><a href="javascript:void(0);">合作联系</a>|</li>
        <li><a href="javascript:void(0);">隐私保护</a>|</li>
        <li><a href="javascript:void(0);">投诉建议</a>|</li>
        <li><a href="javascript:void(0);">免责声明</a>|</li>
        <li><a href="javascript:void(0);">诚聘英才</a></li>
    </ul>
    <p id="foot-1" style="color: #999999; font-size: 12px;">Copyright © 2002-2019 shuyou.com 版权所有
        <a href="javascript:void(0);">ICP证：浙B6-88888888 （金华比奇网络技术有限公司）</a>
        <a href="javascript:void(0);">【网络文化经营许可证】浙网文[2016]0188-088号</a>
    </p>

    <ul id="foot-2-1" style="padding-left: 0;">
        <li><a href="javascript:void(0);">友情合作：</a></li>
        <li><a href="javascript:void(0);">178游戏网</a>|</li>
        <li><a href="javascript:void(0);">新浪游戏频道</a>|</li>
        <li><a href="javascript:void(0);">腾讯游戏频道</a>|</li>
        <li><a href="javascript:void(0);">太平洋游戏网</a>|</li>
        <li><a href="javascript:void(0);">绿盟软件联盟</a>|</li>
        <li><a href="javascript:void(0);">40407网页游戏</a>|</li>
        <li><a href="javascript:void(0);">9U9U网页游戏</a>|</li>
        <li><a href="javascript:void(0);">人人游戏</a>|</li>
        <li><a href="javascript:void(0);">天空下载</a>|</li>
        <li><a href="javascript:void(0);">游戏狗</a>|</li>
        <li><a href="javascript:void(0);">部落冲突</a>|</li>
        <li><a href="javascript:void(0);">2345页游</a>|</li>
        <li><a href="javascript:void(0);">手游客栈</a>|</li>
        <li><a href="javascript:void(0);">51wan游戏</a>|</li>
        <li><a href="javascript:void(0);">贪玩游戏</a>|</li>
        <li><a href="javascript:void(0);">602网页游戏</a>|</li>
        <li><a href="javascript:void(0);">巴士手机游戏</a>|</li>
        <li><a href="javascript:void(0);">ZOL软件下载</a>|</li>
        <li><a href="javascript:void(0);">ZOL手机应用</a>|</li>
        <li><a href="javascript:void(0);">笨游戏</a>|</li>
        <li><a href="javascript:void(0);">17173DNF</a>|</li>
    </ul>
</div>
<script src="plugins/jquery/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>