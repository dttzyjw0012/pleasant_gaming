<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/5 0005
  Time: 下午 3:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>舒游金币交易</title>
    <!--引入bootstrap.min.css文件-->
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap/css/bootstrap.css"/>
    <link href="css/base.css" type="text/css" rel="stylesheet"/>
    <!--引入jquery.min.js文件-->
    <script src="plugins/jquery/jquery.min.js" type="text/javascript"></script>
    <style>
        .bottom-1 {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #3b84e8;
        }

        .middle {
            border: 1px solid white;
            height: 320px;
            width: 100%;
        }

        .panel-title {
            border: 1px solid white;
            height: 20px;
            width: 100px;
        }

        #middle-left-top {
            background-color: white;
            border-color: white;
            padding-left: 30px;
        }

        #middle-left-border {
            border-color: white;
            box-shadow: 0 1px 1px rgba(65, 183, 59, 0);
            margin-left: 100px;
            height: 300px;
        }

        #myCarousel {
            height: 300px;
            width: 600px;
            margin-left: 80px;
            margin-top: 10px;
        }

        #middle-right-top {
            margin-left: 85px;
            margin-top: 20px;
        }

        #middle-right-bottom {
            margin-left: 90px;
            margin-top: 20px;
        }

        #middle-right-down {
            margin-left: 85px;
            margin-top: 10px;
        }

        #bottom {

            margin-top: 340px;
            margin-left: 100px;
            margin-right: 100px;
            height: 480px;

        }

        #bottom-left {
            width: 915px;
        }

        #lol {
            width: 915px;
            height: 400px;
        }

        #mo {
            width: 915px;
            height: 400px;
        }

        #cf {
            width: 915px;
            height: 400px;
        }

        #ji {
            width: 915px;
            height: 400px;
        }

        #bottom-right {
            margin-right: 15px;
        }

        #bottom-right-bottom {
            margin-top: 65px;
            margin-bottom: 0;
        }

        .top-all {
            position: relative;
            height: 25px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            color: #777;
        }

        #top {
            padding-right: 250px;
            padding-left: 250px;
            margin-right: auto;
            margin-left: auto;
        }

        #top-left a {
            height: 50px;
            font-size: 12px;
            line-height: 20px;
            padding: 1px 1px 1px 12px;
        }

        #maxtop a {
            padding-top: 0;
            padding-bottom: 0;
            padding-left: 1px;
        }

        .top-right {
            padding-right: 10px;
        }

        #display:hover ul {
            display: block;
            float: left;
        }

        #top-1 {
            width: 1000px;
            height: 123px;
            padding-top: 15px;
            margin: auto;
        }

        #photo {
            height: 70px;
            width: 315px;
            float: left;
        }

        .search-hovertree-form {
            width: 575px;
            height: 123px;
            overflow: hidden;
        }

        .search-hovertree-form .search-bd {
            height: 25px;
        }

        .search-hovertree-form .search-bd li {
            font-size: 12px;
            width: 60px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            float: left;
            cursor: pointer;
            background-color: #eee;
            color: #666;
        }

        .search-hovertree-form .search-bd li.selected {
            color: #fff;
            font-weight: 700;
            background-color: #3983E8;
        }

        .search-hovertree-form .search-hd {
            height: 34px;
            background-color: #3983E8;
            padding: 3px;
            position: relative;
        }

        .search-hovertree-form .search-hd .search-hovertree-input {
            width: 424px;
            height: 22px;
            line-height: 22px;
            padding: 6px 0;
            background: none;
            text-indent: 10px;
            border: 0;
            outline: none;
            position: relative;
            left: 3px;
            top: 2px;
            z-index: 5;
            #margin-left: -10px;
        }

        .search-hovertree-form .search-hd .btn-search {
            width: 70px;
            height: 34px;
            line-height: 34px;
            position: absolute;
            right: 3px;
            top: 1px;
            border: 0;
            z-index: 6;
            cursor: pointer;
            font-size: 13px;
            color: #fff;
            font-weight: 700;
            background: none;
            outline: none;
        }

        .search-hovertree-form .search-hd .pholder {
            display: inline-block;
            font-size: 12px;
            color: #999;
            position: absolute;
            left: 13px;
            top: 11px;
            z-index: 4;
            padding: 2px 0 2px 25px;
            top: 11px;
        }

        .search-hovertree-form .search-bg {
            width: 350px;
            height: 28px;
            background-color: #fff;
            position: absolute;
            left: 150px;
            top: 3px;
            z-index: 1;
            float: left;
        }

        .choose {
            width: 70px;
            height: 28px;
            float: left;
        }

        #navigator-background {
            width: 100%;
            height: 34px;
            background: url(img/header_repeat.png);
        }

        #navigator-style {
            width: 1000px;
            height: 34px;
            margin: auto;
        }

        #navigator-style li {
            width: 106px;
            height: 34px;
            font-family: arial, fantasy;
            float: left;
            margin: auto;
            line-height: 34px;

        }

        #navigator-style a {
            color: white;
            font-weight: 700;
            font-size: 14px;
        }

        #footer {
            width: 1000px;
            height: 176px;
            padding-top: 20px;
            margin: auto;
        }

        #foot {
            width: 485px;
            height: 18px;
            margin: auto;
            background: white;
        }

        #foot li {
            width: 66.4px;
            height: 18px;
            color: #DDDDDD;
            float: left;
        }

        #foot a {
            width: 48px;
            height: 14.4px;
            margin-right: 8px;
            color: #999999;
        }

        #foot-1 {
            width: 1000px;
            height: 48px;
            padding-top: 15px;
            padding-bottom: 15px;
        }

        #foot-1 a {
            margin-left: 10px;
            color: #999999;
        }

        #foot-2-1 {
            width: 1000px;
            height: 36px;
        }

        #foot-2-1 li {
            float: left;
            color: #DDDDDD;
        }

        #foot-2-1 a {
            margin-left: 8px;
            margin-right: 8px;
            color: #999999;
        }
    </style>
</head>
<body>

<!--导航栏-->
<div class="cantainer-fluid"></div>
<div class="row"></div>
<!--导航栏-->
<nav class="top-all navbar-default" role="navigation" style="margin-bottom: 0;">
    <div id="top">
        <div class="navbar-header" id="top-left">
            <span>欢迎到舒游网络</span><a href="login.jsp">请登陆</a>
            <a href="javascript:void(0);">免费注册</a>
        </div>
        <ul class="nav navbar-nav navbar-right" id="maxtop" style="background-color: white">
            <li class="active">
                <a href="pro_info.jsp" style="padding-left: 15px;padding-top: 2px;">我的舒游</a>
            </li>
            <li>
                <a href="javascript:void(0);"><span class="top-right">|</span>金币兑换</a>
            </li>
            <li>
                <a href="javascript:void(0);"><span class="top-right">|</span>收藏夹</a>
            </li>
            <li>
                <a href="javascript:void(0);"><span class="top-right">|</span>客服中心</a>
            </li>
            <li>
                <a href="javascript:void(0);"><span class="top-right">|</span><img src="img/手机图标.png"/>&nbsp;移动版</a>
            </li>
                <li id="display" class="nav navbar-nav navbar-right">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="top-right">|</span>网站导航 <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" id="dropdown-font">
                        <li class="dropdown-header">商品类</li>
                        <li>
                            <a href="coin_trade.jsp">金币交易</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">装备交易</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">账号交易</a>
                        </li>
                        <li class="divider"></li>
                        <li class="dropdown-header">服务类
                        </li>
                        <li>
                            <a href="javascript:void(0);">资料修改</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">安全中心</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">推广联盟</a>
                        </li>
                    </ul>
                </li>
        </ul>
    </div>
</nav>
<!--搜索栏-->
<div id="top-1">
    <div id="photo">
        <img src="img/logo_140.png" style="padding-top: 16px;"/>
    </div>
    <div class="search-hovertree-form">
        <div id="search-bd" class="search-bd">
            <ul style="background-color: white; margin-bottom: 10px">
                <li class="selected">普通搜索</li>
                <li>简单搜索</li>
            </ul>
        </div>
        <div id="search-hd" class="search-hd">
            <form action="SearchForm" method="GET">
                <select class="choose" style="margin-right: 3px; font-size: 11px" name="gameType"  title="游戏名称">
                    <option value="">游戏名称</option>
                    <option value="">毒奶粉</option>
                    <option value="">英雄联盟</option>
                    <option value="">绝地求生</option>
                    <option value="">穿越火线</option>
                    <option value="">天龙八部</option>
                    <option value="">魔兽世界</option>
                </select>
                <select class="choose" style="font-size: 11px" name="tradeType" title="游戏分类">
                    <option value="">游戏分类</option>
                    <option value="">账号充值</option>
                    <option value="">游戏代练</option>
                    <option value="">账号租赁</option>
                    <option value="">装备</option>
                    <option value="">游戏币</option>
                    <option value="">激活码/CDK</option>
                </select>
                <div class="search-bg"></div>
                <input type="text" id="s1" class="search-hovertree-input" placeholder="请输入任意关键字" name="searchKey">
                <input type="hidden" name="page" value="1">
                <button id="submit" class="btn-search" value="搜索" type="submit">搜索</button>
            </form>
        </div>
    </div>
</div>
<!--<导航栏2-->
<div id="navigator-background">
    <ul id="navigator-style">
        <li style="padding-left: 30px;"><a href="..//" style="padding: 0 0;">首页</a></li>
        <li><a href="coin_trade.jsp" style="padding: 0 0;">金币交易</a></li>
        <li><a href="javascript:void(0);" style="padding: 0 0;">账号交易</a></li>
        <li><a href="javascript:void(0);" style="padding: 0 0;">装备交易</a></li>
        <li><a href="javascript:void(0);" style="padding: 0 0;">手游交易</a></li>
        <li><a href="javascript:void(0);" style="padding: 0 0;">游戏代练</a></li>
        <li><a href="javascript:void(0);" style="padding: 0 0;">点卡交易</a></li>
        <li><a href="javascript:void(0);" style="padding: 0 0;">账号租赁</a></li>
    </ul>
</div>

<div class="middle pull-left">
    <div id="middle-left-border" class="panel panel-default pull-left">
        <div id="middle-left-top" class="panel-heading">
            <h3 class="panel-title">
                热门游戏
            </h3>
        </div>
        <div class="panel-body">
            <img src="img/pro1.jpg"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="img/pro2.jpg"/><br/>
            <img src="img/pro3.jpg"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="img/pro4.jpg"/><br/>
            <img src="img/pro5.jpg"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="img/pro6.jpg"/><br/>
            <img src="img/pro7.jpg"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="img/pro8.jpg"/><br/>
            <img src="img/pro9.jpg"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="img/pro10.jpg"/><br/>
        </div>
    </div>
    <div id="myCarousel" class="carousel slide pull-left">
        <!-- 轮播（Carousel）指标 -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <!-- 轮播（Carousel）项目 -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="img/GE67.jpg" style="height:300px;width:600px;" alt="First slide">
            </div>
            <div class="item">
                <img src="img/gold-1.jpg" style="height:300px;width:600px;" alt="Second slide">
            </div>
            <div class="item">
                <img src="img/gold7.jpg" style="height:300px;width:600px;" alt="Third slide">
            </div>
        </div>
        <!-- 轮播（Carousel）导航 -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="pull-left">
        <div id="middle-right-top" class="btn-group" style="margin-left: 47px">
            <button type="button" class="btn btn-default" style="background-color:gold"><a
                    style="color: white; padding-top: 10px; font-size: 13px">我要买</a></button>
            <button type="button" class="btn btn-default" style="background-color:gold"><a
                    style="color: white; padding-top: 10px; font-size: 13px">我要卖</a></button>
            <button type="button" class="btn btn-default" style="background-color:gold"><a
                    style="color: white; padding-top: 10px; font-size: 13px">金币商城</a></button>
        </div>
        <div id="middle-right-bottom" style="border: 1px solid gold; margin-left: 48px">
            <a href="javascript:void(0);" style="font-size: 16px">购买流程!</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="javascript:void(0);" style="font-size: 16px">出售流程!</a><br/><br/><br/>
            <a href="javascript:void(0);" style="font-size: 16px">咨询专区!</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="javascript:void(0);" style="font-size: 16px">交易安全!</a>
        </div>
        <div id="middle-right-down" class="panel panel-default pull-left"
             style="height: 145px;width: 210px; margin-left: 48px">
            <div class="panel-heading" style="background-color:gold">
                <p style="color: white; font-size: 15px">
                    公告
                </p>
            </div>
            <div class="right-small" style="font-size: 13px">
                    <a href="javascript:void(0);">DNF周年庆大回馈</a>
            </div>
            <div class="right-small" style="font-size: 13px">
                    <a href="javascript:void(0);">LOL皮肤大礼包</a>
            </div>
            <div class="right-small" style="font-size: 13px">
                    <a href="javascript:void(0);">CF新英雄级武器</a>
            </div>
        </div>

    </div>

</div>
<div id="bottom">
    <div id="bottom-left" class="pull-left" style="margin-top: 20px; font-size: 14px">
        <div id="bottom-left-top">
            <ul id="myTab" class="nav nav-tabs bottom-1">
                <li style="color: white">
                    <a href="#cf" data-toggle="tab" class="active" style="color: black">
                        地下城
                    </a>
                </li>
                <li>
                    <a href="#mo" data-toggle="tab" style="color: black">魔兽世界</a>
                </li>
                <li>
                    <a href="#lol" data-toggle="tab" style="color: black">剑灵</a>
                </li>
                <li>
                    <a href="#ji" data-toggle="tab" style="color: black">天龙八部</a>
                </li>
                <li style="margin-top: 10px;margin-right: 10PX; float: right !important; color: black;">热门游戏</li>
            </ul>
        </div>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="cf">
                <div class="pull-left">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>

                <div class="pull-left" style="margin-top: 50px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/pro1.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="mo">
                <div class="pull-left">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>

                <div class="pull-left" style="margin-top: 50px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGY9AAAAAAAABbhhDo9Vo57.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
            </div>
            <div class="tab-pane fade" id="lol">
                <div class="pull-left">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>

                <div class="pull-left" style="margin-top: 50px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjZG3kAAAAAAAAUEmi049U46.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>

            </div>
            <div class="tab-pane fade" id="ji">
                <div class="pull-left">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>

                <div class="pull-left" style="margin-top: 50px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
                <div class="pull-left" style="margin-top: 50px;margin-left: 87px;">
                    <a href="javascript:void(0);"><img src="img/SgKowFjGUvAAAAAAAAB0DF8aGO038.jpg"/></a><br/>
                    <p style="margin-left: 10px;margin-bottom: 0;">区：黑龙江区</p>
                    <p style="margin-left: 10px;margin-bottom: 0;">服：血雨腥风</p>
                    <a style="margin-left: 10px;" href="javascript:void(0);">一元=1000金币</a>
                    <p style="margin-left: 10px;">特价 ￥10.00</p>
                    <a href="javascript:void(0);">
                        <button type="button" class="btn btn-danger" style="margin-left: 20px;">立即抢</button>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <div id="bottom-right" class="panel panel-default pull-right"
         style="height: 250px;width: 210px; margin-top: 20px; margin-left: -20px">
        <div class="panel-heading" style="font-size: 15px">
            游戏币价格指数查询
        </div>
        <form role="form">
            <div class="form-group">
                <label for="form-group-select-a"><span class="pull-left"
                                                       style="margin-top: 8px;margin-left: 5px;">游戏名称</span></label>
                <select id="form-group-select-a" class="form-control pull-right" style="width: 120px;">
                    <option>选择游戏</option>
                    <option>穿越火线</option>
                    <option>英雄联盟</option>
                    <option>地下城</option>
                    <option>魔兽世界</option>
                    <option>古剑奇谭</option>
                </select>
            </div>
        </form>
        <form role="form">
            <div class="form-group">
                <label for="form-group-select-b"><span class="pull-left"
                                                       style="margin-top: -8px;margin-left: 5px;">游戏区服</span></label>
                <select id="form-group-select-b" class="form-control pull-right"
                        style="width: 120px;margin-top: -15px;">
                    <option>选择游戏区</option>
                    <option>黑龙江区</option>
                    <option>辽宁区</option>
                    <option>吉林区</option>
                    <option>河北区</option>
                </select>

            </div>
        </form>
        <form role="form">
            <div class="form-group">

                <select class="form-control pull-right" style="width: 120px;margin-top: -15px;" title="选择游戏服">
                    <option>选择游戏服</option>
                    <option>王者独尊</option>
                    <option>不羁之风</option>
                    <option>风华绚烂</option>
                    <option>激情迸发</option>
                </select>

            </div>
        </form>
        <form role="form">
            <div class="form-group">
                <label for="form-group-select-c"><span class="pull-left" style="margin-top: 30px;margin-left: 5px;">游戏币单位</span></label>
                <select id="form-group-select-c" class="form-control pull-right"
                        style="width: 120px;margin-top: -38px;">
                    <option>游戏币单位</option>
                    <option>Q点</option>
                    <option>点劵</option>
                    <option>金币</option>
                </select>

            </div>
        </form>
        <button type="button" class="btn btn-info"><a href="javascript:void(0);">查询</a></button>
        <div id="bottom-right-bottom" class="panel panel-default" style="height: 145px;width: 210px;">
            <div class="panel-heading" style="font-size: 14px">
                卖家专区
            </div>
            <div class="right-small">
                <span style="font-size: 13px">
                    <a href="javascript:void(0);">游戏币服务费收费标准</a>
                </span>
            </div>
            <div class="right-small">
                <span style="font-size: 13px">
                    <a href="javascript:void(0);">超时赔付标准</a>
                </span>
            </div>
            <div class="right-small">
                <span style="font-size: 13px">
                    <a href="javascript:void(0);">寄售卖家如何联系接受客服</a>
                </span>
            </div>
        </div>
    </div>
</div>
<div id="underneath"
     style="background-color: #F7F7F7;height: 200px;margin-left: 100px;margin-right: 100px;font-size: 14px">
    <div style="margin-top:0;margin-bottom: 10px;margin-left: 100px;">舒游三大独家交易模式</div>
    <div style="margin-left: 100px;">

        <div class="pull-left"><img src="img/deal1.png"></div>
        <div class="pull-left" style="margin-left: 10px;">
            什么是寄售交易<br/>
            寄售交易是指卖家将现存货物的游戏账<br>号暂时寄存在舒游，有买家购买时，由<br>客服人员直接登录游戏进行发货，无需<br>卖家整天守候、上线配合的交易方式。
        </div>

    </div>
    <div style="margin-left: 100px;" class="pull-left">
        <div class="pull-left"><img src="img/deal2.png"></div>
        <div class="pull-left" style="margin-left: 10px;">
            什么是担保交易<br/>
            担保交易，卖家只需在舒游网站上发布<br>担保出售信息，不需填写游戏帐号资<br>料，交易时由舒游客服通知卖家登录游<br>戏，与买家交易的一种全新交易模式。
        </div>

    </div>
    <div style="margin-left: 100px;" class="pull-left">
        <div class="pull-left"><img src="img/deal3.png"></div>
        <div class="pull-left" style="margin-left: 10px;">
            什么是小额交易<br/>
            小额交易是指出售单价在50元以下的游<br>戏币和装备交易，服务费享受特惠，仅<br>收取交易金额10%的一种交易。小额交<br>易支持寄售交易、担保交易两种方式。
        </div>

    </div>
</div>

<div id="footer">
    <ul id="foot">
        <li><a href="javascript:void(0);">关于我们</a>|</li>
        <li><a href="javascript:void(0);">合作伙伴</a>|</li>
        <li><a href="javascript:void(0);">合作联系</a>|</li>
        <li><a href="javascript:void(0);">隐私保护</a>|</li>
        <li><a href="javascript:void(0);">投诉建议</a>|</li>
        <li><a href="javascript:void(0);">免责声明</a>|</li>
        <li><a href="javascript:void(0);">诚聘英才</a></li>
    </ul>
    <p id="foot-1" style="color: #999999;">Copyright © 2002-2019 shuyou.com 版权所有
        <a href="javascript:void(0);">ICP证：浙B6-88888888 （金华比奇网络技术有限公司）</a>
        <a href="javascript:void(0);">【网络文化经营许可证】浙网文[2016]0188-088号</a>
    </p>
    <!--<div id="foot-2">-->
    <ul id="foot-2-1">
        <li><a href="javascript:void(0);">友情合作：</a></li>
        <li><a href="javascript:void(0);">178游戏网</a>|</li>
        <li><a href="javascript:void(0);">新浪游戏频道</a>|</li>
        <li><a href="javascript:void(0);">腾讯游戏频道</a>|</li>
        <li><a href="javascript:void(0);">太平洋游戏网</a>|</li>
        <li><a href="javascript:void(0);">绿盟软件联盟</a>|</li>
        <li><a href="javascript:void(0);">40407网页游戏</a>|</li>
        <li><a href="javascript:void(0);">9U9U网页游戏</a>|</li>
        <li><a href="javascript:void(0);">人人游戏</a>|</li>
        <li><a href="javascript:void(0);">天空下载</a>|</li>
        <li><a href="javascript:void(0);">游戏狗</a>|</li>
        <li><a href="javascript:void(0);">部落冲突</a>|</li>
        <li><a href="javascript:void(0);">2345页游</a>|</li>
        <li><a href="javascript:void(0);">手游客栈</a>|</li>
        <li><a href="javascript:void(0);">51wan游戏</a>|</li>
        <li><a href="javascript:void(0);">贪玩游戏</a>|</li>
        <li><a href="javascript:void(0);">602网页游戏</a>|</li>
        <li><a href="javascript:void(0);">巴士手机游戏</a>|</li>
        <li><a href="javascript:void(0);">ZOL软件下载</a>|</li>
        <li><a href="javascript:void(0);">ZOL手机应用</a>|</li>
        <li><a href="javascript:void(0);">笨游戏</a>|</li>
        <li><a href="javascript:void(0);">17173DNF</a>|</li>
    </ul>


</div>

<script type="text/javascript">$(function () {
    //通用头部搜索切换
    $('#search-hd').find('.search-hovertree-input').on('input propertychange', function () {
        let val = $(this).val();
        if (val.length > 0) {
            $('#search-hd').find('.pholder').hide(0);
        }
        else {
            // language=JQuery-CSS
            let index;
            index = $('#search-bd').find('li.selected').index();
            $('#search-hd').find('.pholder').eq(index).show().siblings('.pholder').hide(0);
        }
    });
    $('#search-bd').find('li').click(function () {
        let index = $(this).index();
        let search_hd = $('#search-hd');
        search_hd.find('.pholder').eq(index).show().siblings('.pholder').hide(0);
        search_hd.find('.search-hover' + 'tree-input').eq(index).show().siblings('.search-hovertree-input').hide(0);
        $(this).addClass('selected').siblings().removeClass('selected');
        search_hd.find('.search-hovertree-input').val('');
    });
});

</script>
<!--引入bootstrap.min.js文件-->
<script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>