<%@ page import="com.comfortable_game.entity.OrderInfo" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>DNF</title>

    <!--引入bootstrap.min.css文件-->
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap/css/bootstrap.css"/>
    <link href="css/base.css" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" href="plugins/assets/css/app.css">

    <style type="text/css">
        #display > li:hover ul {
            display: block;
            float: left;
        }

        #top {
            padding-right: 250px;
            padding-left: 250px;
            margin-right: auto;
            margin-left: auto;
        }

        #top-left a {
            height: 50px;
            font-size: 12px;
            line-height: 20px;
            padding: 1px 1px 1px 12px;
        }

        .top-all {
            position: relative;
            height: 25px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            color: #777;
        }

        #maxtop a {
            padding-top: 0;
            padding-bottom: 0;
            padding-left: 1px;
        }

        .top-right {
            padding-right: 10px;
        }

        #dropdown-font a {
            font-size: 13px;
            padding-left: 10px;
        }

        .search-hovertree-form {
            width: 575px;
            height: 123px;
            overflow: hidden;
        }

        .search-hovertree-form .search-bd {
            height: 25px;
        }

        .search-hovertree-form .search-bd li {
            font-size: 12px;
            width: 60px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            float: left;
            cursor: pointer;
            background-color: #eee;
            color: #666;
        }

        .search-hovertree-form .search-bd li.selected {
            color: #fff;
            font-weight: 700;
            background-color: #3983E8;
        }

        .search-hovertree-form .search-hd {
            height: 34px;
            background-color: #3983E8;
            padding: 3px;
            position: relative;
        }

        .search-hovertree-form .search-hd .search-hovertree-input {
            width: 424px;
            height: 22px;
            line-height: 22px;
            padding: 6px 0;
            background: none;
            text-indent: 10px;
            border: 0;
            outline: none;
            position: relative;
            left: 3px;
            top: 2px;
            z-index: 5;
            #margin-left: -10px;
        }

        .search-hovertree-form .search-hd .btn-search {
            width: 70px;
            height: 34px;
            line-height: 34px;
            position: absolute;
            right: 3px;
            top: 1px;
            border: 0;
            z-index: 6;
            cursor: pointer;
            font-size: 13px;
            color: #fff;
            font-weight: 700;
            background: none;
            outline: none;
        }

        .search-hovertree-form .search-hd .pholder {
            display: inline-block;
            font-size: 12px;
            color: #999;
            position: absolute;
            left: 13px;
            top: 11px;
            z-index: 4;
            padding: 2px 0 2px 25px;
            top: 11px;
        }

        .search-hovertree-form .search-bg {
            width: 350px;
            height: 28px;
            background-color: #fff;
            position: absolute;
            left: 150px;
            top: 3px;
            z-index: 1;
            float: left;
        }

        #top-1 {
            width: 1000px;
            height: 123px;
            padding-top: 15px;
            margin: auto;
        }

        #photo {
            height: 70px;
            width: 315px;
            float: left;
        }

        .choose {
            width: 70px;
            height: 28px;
            float: left;
        }

        #out {
            width: 950px;
            height: 1120px;
            margin: auto;
            /*				border: 1px solid red;*/
        }

        #head {
            width: 950px;
            height: 32px;
            text-align: left;

        }

        #head a {
            color: cornflowerblue;
        }

        #head-right {
            float: right;
        }

        #head-right span {
            color: orange;
        }

        #head-1 {
            width: 950px;
            height: 32px;
            background-color: #f1f8fc;
            border: 1px solid #aed3ef;
            text-align: left;
            padding-top: 6px;
            padding-left: 10px;
        }

        #head-1-right {
            width: 72px;
            height: 20px;
            color: #999;
            margin-right: 10px;
            float: right;
            border-bottom: 1px solid #999999;
        }

        #suit {
            width: 950px;
            border: 1px solid #aed3ef;
            margin-top: 10px;
            float: left;
        }

        .type {
            width: 120px;
            height: 24px;
            color: #06c;
            float: left;
        }

        .suit li {
            width: 140px;
            height: 24px;
            float: left;
        }

        #suit a {
            color: #06c;
        }

        .myBtn-style {
            background: #f1f8fc;
            border-radius: 5px;

        }

        .ss-style {
            width: 700px;
        }

        #shorts {
            width: 950px;
        }

        .form {
            width: 950px;
            height: 32px;
            float: left;
            padding-top: 4px;
            padding-left: 15px;
            text-align: left;
            background-color: #fbfbfb;
        }

        .form-1 {
            width: 950px;
            height: 32px;
            float: left;
            padding-top: 4px;
            padding-left: 15px;
            text-align: left;
            background-color: #f6f6f6;
        }

        #all {
            height: 65px;
            border: 1px solid #c6e0f4;
            float: left;
        }

        #short-out {
            width: 950px;
            margin-top: 70px;
            text-align: left;
        }

        #shorts-1 {
            width: 950px;
            height: 20px;
            margin-top: 10px;
        }

        #shorts-1 li {
            float: left;
        }

        #order {
            width: 505px;
            height: 30px;
            text-align: left;
            padding-left: 15px;
        }

        #price {
            width: 90px;
            height: 30px;
            text-align: center;
        }

        #num {
            width: 90px;
            height: 30px;
            text-align: center;
        }

        #serve {
            width: 155px;
            height: 30px;
            text-align: center;
        }

        .list-1 {
            width: 950px;
            height: 120px;
            border-bottom: 0.5px solid #eee;
            border-top: 0.5px solid #eee;
            padding-top: 12px;
            padding-bottom: 12px;
        }

        .list-order {
            width: 505px;
            height: 100px;
            float: left;
        }

        .list-price {
            width: 90px;
            height: 23px;
            float: left;
            text-align: center;
        }

        .list-num {
            width: 90px;
            height: 23px;
            float: left;
            text-align: center;
        }

        .list-serve {
            width: 155px;
            height: 88px;
            float: left;
            margin-left: 40px;

        }

        #footer {
            width: 1000px;
            height: 176px;
            margin: auto;
        }

        #foot {
            width: 485px;
            height: 18px;
            margin: auto;
        }

        #foot li {
            width: 66.4px;
            height: 18px;
            color: #DDDDDD;
            float: left;
        }

        #foot a {
            width: 48px;
            height: 14.4px;
            margin-right: 8px;
            color: #999999;
        }

        #foot-1 {
            width: 1000px;
            height: 48px;
            padding-top: 15px;
            padding-bottom: 15px;
        }

        #foot-1 a {
            margin-left: 10px;
            color: #999999;
        }

        #foot-2-1 {
            width: 1000px;
            height: 36px;
        }

        #foot-2-1 li {
            float: left;
            color: #DDDDDD;
        }

        #foot-2-1 a {
            margin-left: 8px;
            margin-right: 8px;
            color: #999999;
        }

        .type-select {
            float: left;
            width: 710px;
        }

        .type {
            text-align: left;
        }

        .ss.ss-style {
            display: none;
        }

        .ss-1.ss-style {
            display: none;
        }
    </style>
</head>
<body>
<div class="cantainer-fluid"></div>
<div class="row"></div>
<!--导航栏-->
<nav class="top-all navbar-default" role="navigation" style="margin-bottom: 0;">
    <div id="top">
        <div class="navbar-header" id="top-left">
            <span>欢迎到舒游网络</span><a href="login.jsp">请登陆</a>
            <a href="#">免费注册</a>
        </div>
        <ul class="nav navbar-nav navbar-right" id="maxtop">
            <li class="active">
                <a href="pro_info.jsp" style="padding-left: 15px;padding-top: 2px;">我的舒游</a>
            </li>
            <li>
                <a href="#"><span class="top-right">|</span>金币兑换</a>
            </li>
            <li>
                <a href="#"><span class="top-right">|</span>收藏夹</a>
            </li>
            <li>
                <a href="#"><span class="top-right">|</span>客服中心</a>
            </li>
            <li>
                <a href="#"><span class="top-right">|</span><img src="img/手机图标.png"/>&nbsp;移动版</a>
            </li>
            <li id="display" class="nav navbar-nav navbar-right">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="top-right">|</span>网站导航 <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" id="dropdown-font">
                    <li class="dropdown-header">商品类</li>
                    <li>
                        <a href="coin_trade.jsp">金币交易</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">装备交易</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">账号交易</a>
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">服务类
                    </li>
                    <li>
                        <a href="javascript:void(0);">资料修改</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">安全中心</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">推广联盟</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<!--搜索栏-->
<div id="top-1">
    <div id="photo">
        <img src="img/logo_140.png" style="padding-top: 16px;"/>
    </div>
    <div class="search-hovertree-form">
        <div id="search-bd" class="search-bd">
            <ul style="background-color: white; margin-bottom: 10px">
                <li class="selected">普通搜索</li>
                <li>简单搜索</li>
            </ul>
        </div>
        <div id="search-hd" class="search-hd">
            <form action="SearchForm" method="GET">
                <select class="choose" style="margin-right: 3px; font-size: 11px" name="gameType" title="游戏名称">
                    <option value="">游戏名称</option>
                    <option value="">毒奶粉</option>
                    <option value="">英雄联盟</option>
                    <option value="">绝地求生</option>
                    <option value="">穿越火线</option>
                    <option value="">天龙八部</option>
                    <option value="">魔兽世界</option>
                </select>
                <select class="choose" style="font-size: 11px" name="tradeType" title="游戏分类">
                    <option value="">游戏分类</option>
                    <option value="">账号充值</option>
                    <option value="">游戏代练</option>
                    <option value="">账号租赁</option>
                    <option value="">装备</option>
                    <option value="">游戏币</option>
                    <option value="">激活码/CDK</option>
                </select>
                <div class="search-bg"></div>
                <input type="text" id="s1" class="search-hovertree-input" placeholder="请输入任意关键字" name="searchKey">
                <input type="hidden" name="page" value="1">
                <button id="submit" class="btn-search" value="搜索" type="submit">搜索</button>
            </form>
        </div>
    </div>
</div>
<!--   内容-->
<div id="out">
    <!--你的位置-->
    <div id="head">
        <span>您的位置：</span>
        <a href="..//">首页</a>>
        <a href="#">搜索</a>
        <%!
            @SuppressWarnings("unchecked")
            private static <T> T castAttribute(Object obj) {
                return (T) obj;
            }
        %>
        <%
            String searchKey = (String) request.getAttribute("searchKey");
            Integer searchPage = (Integer) request.getAttribute("searchPage");
            List<OrderInfo> orderInfosBySearch = castAttribute(request.getAttribute("orderInfosBySearch"));
        %>
        <div id="head-right" onclick="test()">
            为你找到：<span><%=searchKey%></span>相关商品&nbsp;<span><%=orderInfosBySearch.size()%></span>件
        </div>
    </div>
    <!--你选择了....-->
    <div id="head-1">
        您选择了：<a href="#" style="border: 1px solid blue;"><%=searchKey%>√</a>
        <a href="#" id="head-1-right">删除所有选择</a>
    </div>
    <!--商品类型选择模块-->
    <div id="suit">
        <div>
            <div class="type"><p>&nbsp;&nbsp;&nbsp;&nbsp;商品类型选择：</p></div>
            <div class="type-select">
                <ul class="suit">
                    <li><a href="#">游戏币</a></li>
                    <li><a href="#">挑战书</a></li>
                    <li><a href="#">游戏币批发</a></li>
                    <li><a href="#">游戏账号</a></li>
                    <li><a href="#">游戏代练</a></li>
                    <li><a href="#">装备</a></li>
                    <li><a href="#">增幅卷/强化卷</a></li>
                    <li><a href="#">宝珠</a></li>
                    <li><a href="#">称号</a></li>
                    <li><a href="#">勇士推荐点</a></li>
                    <li class="ss ss-style"><a href="#">矛盾得结晶体</a></li>
                    <li class="ss ss-style"><a href="#">魔刹石</a></li>
                    <li class="ss ss-style"><a href="#">无色小晶块</a></li>
                    <li class="ss ss-style"><a href="#">点卡</a></li>
                    <li class="ss ss-style"><a href="#">材料</a></li>
                    <li class="ss ss-style"><a href="#">点卷</a></li>
                    <li class="ss ss-style"><a href="#">激活码/CDK</a></li>
                    <li class="ss ss-style"><a href="#">激活码</a></li>
                    <li class="ss ss-style"><a href="#">其他</a></li>
                    <li class="ss ss-style"><a href="#">直冲点卷</a></li>
                    <li class="ss ss-style"><a href="#">QB</a></li>
                    <li class="ss ss-style"><a href="#">十周年站街活动</a></li>
                    <li class="ss ss-style"><a href="#">预约号升级</a></li>
                    <li class="ss ss-style"><a href="#">安图恩攻坚团</a></li>
                    <li class="ss ss-style"></li>
                    <li></li>
                    <li></li>
                    <li>
                        <button class="myBtn myBtn-style" onclick="myFunction()">显示全部</button>
                    </li>
                </ul>
            </div>
        </div>
        <div>
            <div class="type"><p>&nbsp;&nbsp;&nbsp;&nbsp;游戏区选择：</p></div>
            <div class="type-select">
                <ul class="suit">
                    <li><a href="#">东北区</a></li>
                    <li><a href="#">黑龙江区</a></li>
                    <li><a href="#">华北区</a></li>
                    <li><a href="#">广东区</a></li>
                    <li><a href="#">北京区</a></li>
                    <li><a href="#">浙江区</a></li>
                    <li><a href="#">广西区</a></li>
                    <li><a href="#">西南区</a></li>
                    <li><a href="#">陕西区</a></li>
                    <li><a href="#">湖南区</a></li>
                    <li class="ss-1 ss-style"><a href="#">安徽区</a></li>
                    <li class="ss-1 ss-style"><a href="#">北京区</a></li>
                    <li class="ss-1 ss-style"><a href="#">重庆区</a></li>
                    <li class="ss-1 ss-style"><a href="#">吉林区</a></li>
                    <li class="ss-1 ss-style"><a href="#">新疆区</a></li>
                    <li class="ss-1 ss-style"><a href="#">四川区</a></li>
                    <li class="ss-1 ss-style"><a href="#">河北区</a></li>
                    <li class="ss-1 ss-style"><a href="#">山西区</a></li>
                    <li class="ss-1 ss-style"><a href="#">内蒙古区</a></li>
                    <li class="ss-1 ss-style"><a href="#">江苏区</a></li>
                    <li class="ss-1 ss-style"><a href="#">山东区</a></li>
                    <li class="ss-1 ss-style"><a href="#">辽宁区</a></li>
                    <li class="ss-1 ss-style"><a href="#">天津区</a></li>
                    <li class="ss-1 ss-style"><a href="#">云贵区</a></li>
                    <li class="ss-1 ss-style"><a href="#">湖北区</a></li>
                    <li class="ss-1 ss-style"><a href="#">上海区</a></li>
                    <li class="ss-1 ss-style"><a href="#">福建区</a></li>
                    <li class="ss-1 ss-style"><a href="#">西北区</a></li>
                    <li class="ss-1 ss-style"><a href="#">河南区</a></li>
                    <li class="ss-1 ss-style"><a href="#">体验区</a></li>
                    <li></li>
                    <li></li>
                    <li>
                        <button class="myBtn-1 myBtn-style" onclick="myFunction1()">显示全部</button>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--从标签开始-->
    <div id="shorts">
        <ul id="myTab" class="nav nav-tabs tab">
            <li class="active">
                <a href="#all" data-toggle="tab">全部商品</a>
            </li>
            <li>
                <a href="#ios" data-toggle="tab">游戏币</a>
            </li>
            <li>
                <a href="#ios" data-toggle="tab">挑战书</a>
            </li>
            <li>
                <a href="#ios" data-toggle="tab">游戏账号</a>
            </li>
            <li>
                <a href="#ios" data-toggle="tab">游戏代练</a>
            </li>
            <li>
                <a href="#ios" data-toggle="tab">装备</a>
            </li>
            <li class="dropdown">
                <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown">更多
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
                    <li>
                        <a href="#jmeter" tabindex="-1" data-toggle="tab">增幅卷/强化卷</a>
                    </li>
                    <li>
                        <a href="#ejb" tabindex="-1" data-toggle="tab">宝珠</a>
                    </li>
                    <li>
                        <a href="#ejb" tabindex="-1" data-toggle="tab">称号</a>
                    </li>
                    <li>
                        <a href="#ejb" tabindex="-1" data-toggle="tab">魔刹石</a>
                    </li>
                    <li>
                        <a href="#ejb" tabindex="-1" data-toggle="tab">点卡</a>
                    </li>
                </ul>
            </li>
        </ul>

        <div id="myTabContent" class="tab-content">

            <div class="tab-pane fade in active" id="all">

                <form class="form" name="input" action="#" method="get">
                    关键字: <input type="text" name="user" title="关键字">
                    <input type="submit" value="确定">

                    价格：<input style="width: 30px;" type="text" name="user" title="价格">
                    <span>—</span>
                    <input style="width: 30px;" type="text" name="user" title="价格">
                    <input type="submit" value="确定">
                </form>

                <form class="form-1" name="input" action="#" method="get">只显示：
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" value="option1"> 寄售交易
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox2" value="option2"> 担保交易
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox3" value="option3"> 无货赔付
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox4" value="option3"> 至尊宝改人脸
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox5" value="option3"> 本商品可投保
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox6" value="option3"> 找回包赔
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox7" value="option3"> 本商品已投保
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox8" value="option3"> 身份证
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox9" value="option3"> 安心买
                    </label>
                </form>
                <!--商品列表-->
                <div id="short-out">
                    <ul id="shorts-1">
                        <li id="order"><a href="#">默认排序</a></li>
                        <li id="price"><a href="#">价格</a></li>
                        <li id="num"><a href="#">库存</a></li>
                        <li id="serve"><a href="#">服务保障类型</a></li>
                    </ul>


                    <%
                        int itemZeroSerial = (searchPage - 1) * 6;
                        if (itemZeroSerial < orderInfosBySearch.size()) {
                    %>
                    <div id="list-1" class="list-1">
                        <ul class="list-order">
                            <li><h5 style="margin: 0;"><a href="#"
                                                          style="color: #06c;"><%=orderInfosBySearch.get(itemZeroSerial).getTradeName()%>
                            </a></h5></li>
                            <li>商品类型：<a href="#"><%=orderInfosBySearch.get(itemZeroSerial).getTradeTypeId()%>
                            </a></li>
                            <li>游戏/区/服：<a href="#"><%=orderInfosBySearch.get(itemZeroSerial).getGameNameId()%>
                            </a>/<a href="#"><%=orderInfosBySearch.get(itemZeroSerial).getGameServerName()%>
                            </a></li>

                        </ul>
                        <ul class="list-price">
                            <li>
                                <strong><%=String.format("%.2f", orderInfosBySearch.get(itemZeroSerial).getPrice())%>
                                </strong></li>
                        </ul>
                        <ul class="list-num">
                            <li><%=orderInfosBySearch.get(itemZeroSerial).getInventoryQuantity()%>
                            </li>
                        </ul>
                        <ul class="list-serve">
                            <li><a href="#">无货赔付</a></li>
                            <li><a href="#">至尊宝改人脸</a></li>
                            <li><a href="#">免单购物服务</a></li>
                            <li><a href="#">本商品可投保</a></li>
                            <li><a href="#">安心买</a></li>
                        </ul>
                    </div>
                    <%
                        } else {

                        }
                    %>

                    <%
                        int itemOneSerial = 1 + (searchPage - 1) * 6;
                        if (itemOneSerial < orderInfosBySearch.size()) {
                    %>
                    <div id="list-2" class="list-1" style="background-color: rgb(251,251,251);">
                        <ul class="list-order">
                            <li><h5 style="margin: 0;"><a href="#"
                                                          style="color: #06c;"><%=orderInfosBySearch.get(itemOneSerial).getTradeName()%>
                            </a></h5></li>
                            <li>商品类型：<a href="#"><%=orderInfosBySearch.get(itemOneSerial).getTradeTypeId()%>
                            </a></li>
                            <li>游戏/区/服：<a href="#"><%=orderInfosBySearch.get(itemOneSerial).getGameNameId()%>
                            </a>/<a href="#"><%=orderInfosBySearch.get(itemOneSerial).getGameServerName()%>
                            </a></li>

                        </ul>
                        <ul class="list-price">
                            <li>
                                <strong><%=String.format("%.2f", orderInfosBySearch.get(itemOneSerial).getPrice())%>
                                </strong></li>
                        </ul>
                        <ul class="list-num">
                            <li><%=orderInfosBySearch.get(itemOneSerial).getInventoryQuantity()%>
                            </li>
                        </ul>
                        <ul class="list-serve">
                            <li><a href="#">无货赔付</a></li>
                            <li><a href="#">至尊宝改人脸</a></li>
                            <li><a href="#">免单购物服务</a></li>
                            <li><a href="#">本商品可投保</a></li>
                            <li><a href="#">安心买</a></li>
                        </ul>
                    </div>
                    <%
                        } else {

                        }
                    %>

                    <%
                        int itemTwoSerial = 2 + (searchPage - 1) * 6;
                        if (itemTwoSerial < orderInfosBySearch.size()) {
                    %>
                    <div id="list-3" class="list-1">
                        <ul class="list-order">
                            <li><h5 style="margin: 0;"><a href="#"
                                                          style="color: #06c;"><%=orderInfosBySearch.get(itemTwoSerial).getTradeName()%>
                            </a></h5></li>
                            <li>商品类型：<a href="#"><%=orderInfosBySearch.get(itemTwoSerial).getTradeTypeId()%>
                            </a></li>
                            <li>游戏/区/服：<a href="#"><%=orderInfosBySearch.get(itemTwoSerial).getGameNameId()%>
                            </a>/<a href="#"><%=orderInfosBySearch.get(itemTwoSerial).getGameServerName()%>
                            </a></li>

                        </ul>
                        <ul class="list-price">
                            <li>
                                <strong><%=String.format("%.2f", orderInfosBySearch.get(itemTwoSerial).getPrice())%>
                                </strong></li>
                        </ul>
                        <ul class="list-num">
                            <li><%=orderInfosBySearch.get(itemTwoSerial).getInventoryQuantity()%>
                            </li>
                        </ul>
                        <ul class="list-serve">
                            <li><a href="#">无货赔付</a></li>
                            <li><a href="#">至尊宝改人脸</a></li>
                            <li><a href="#">免单购物服务</a></li>
                            <li><a href="#">本商品可投保</a></li>
                            <li><a href="#">安心买</a></li>
                        </ul>
                    </div>
                    <%
                        } else {

                        }
                    %>

                    <%
                        int itemThreeSerial = 3 + (searchPage - 1) * 6;
                        if (itemThreeSerial < orderInfosBySearch.size()) {
                    %>
                    <div id="list-4" class="list-1" style="background-color: rgb(251,251,251);">
                        <ul class="list-order">
                            <li><h5 style="margin: 0;"><a href="#"
                                                          style="color: #06c;"><%=orderInfosBySearch.get(itemThreeSerial).getTradeName()%>
                            </a></h5></li>
                            <li>商品类型：<a href="#"><%=orderInfosBySearch.get(itemThreeSerial).getTradeTypeId()%>
                            </a></li>
                            <li>游戏/区/服：<a href="#"><%=orderInfosBySearch.get(itemThreeSerial).getGameNameId()%>
                            </a>/<a href="#"><%=orderInfosBySearch.get(itemThreeSerial).getGameServerName()%>
                            </a></li>

                        </ul>
                        <ul class="list-price">
                            <li><strong><%=String.format("%.2f", orderInfosBySearch.get(3).getPrice())%>
                            </strong></li>
                        </ul>
                        <ul class="list-num">
                            <li><%=orderInfosBySearch.get(itemThreeSerial).getInventoryQuantity()%>
                            </li>
                        </ul>
                        <ul class="list-serve">
                            <li><a href="#">无货赔付</a></li>
                            <li><a href="#">至尊宝改人脸</a></li>
                            <li><a href="#">免单购物服务</a></li>
                            <li><a href="#">本商品可投保</a></li>
                            <li><a href="#">安心买</a></li>
                        </ul>
                    </div>
                    <%
                        } else {

                        }
                    %>

                    <%
                        int itemFourSerial = 4 + (searchPage - 1) * 6;
                        if (itemFourSerial < orderInfosBySearch.size()) {
                    %>
                    <div id="list-5" class="list-1">
                        <ul class="list-order">
                            <li><h5 style="margin: 0;"><a href="#"
                                                          style="color: #06c;"><%=orderInfosBySearch.get(itemFourSerial).getTradeName()%>
                            </a></h5></li>
                            <li>商品类型：<a href="#"><%=orderInfosBySearch.get(itemFourSerial).getTradeTypeId()%>
                            </a></li>
                            <li>游戏/区/服：<a href="#"><%=orderInfosBySearch.get(itemFourSerial).getGameNameId()%>
                            </a>/<a href="#"><%=orderInfosBySearch.get(itemFourSerial).getGameServerName()%>
                            </a></li>

                        </ul>
                        <ul class="list-price">
                            <li>
                                <strong><%=String.format("%.2f", orderInfosBySearch.get(itemFourSerial).getPrice())%>
                                </strong></li>
                        </ul>
                        <ul class="list-num">
                            <li><%=orderInfosBySearch.get(itemFourSerial).getInventoryQuantity()%>
                            </li>
                        </ul>
                        <ul class="list-serve">
                            <li><a href="#">无货赔付</a></li>
                            <li><a href="#">至尊宝改人脸</a></li>
                            <li><a href="#">免单购物服务</a></li>
                            <li><a href="#">本商品可投保</a></li>
                            <li><a href="#">安心买</a></li>
                        </ul>
                    </div>
                    <%
                        } else {

                        }
                    %>

                    <%
                        int itemFiveSerial = 5 + (searchPage - 1) * 6;
                        if (itemFiveSerial < orderInfosBySearch.size()) {
                    %>
                    <div id="list-6" class="list-1" style="background-color: rgb(251,251,251);">
                        <ul class="list-order">
                            <li><h5 style="margin: 0;"><a href="#"
                                                          style="color: #06c;"><%=orderInfosBySearch.get(itemFiveSerial).getTradeName()%>
                            </a></h5></li>
                            <li>商品类型：<a href="#"><%=orderInfosBySearch.get(itemFiveSerial).getTradeTypeId()%>
                            </a></li>
                            <li>游戏/区/服：<a href="#"><%=orderInfosBySearch.get(itemFiveSerial).getGameNameId()%>
                            </a>/<a href="#"><%=orderInfosBySearch.get(itemFiveSerial).getGameServerName()%>
                            </a></li>

                        </ul>
                        <ul class="list-price">
                            <li>
                                <strong><%=String.format("%.2f", orderInfosBySearch.get(itemFiveSerial).getPrice())%>
                                </strong></li>
                        </ul>
                        <ul class="list-num">
                            <li><%=orderInfosBySearch.get(itemFiveSerial).getInventoryQuantity()%>
                            </li>
                        </ul>
                        <ul class="list-serve">
                            <li><a href="#">无货赔付</a></li>
                            <li><a href="#">至尊宝改人脸</a></li>
                            <li><a href="#">免单购物服务</a></li>
                            <li><a href="#">本商品可投保</a></li>
                            <li><a href="#">安心买</a></li>
                        </ul>
                    </div>
                    <%
                        } else {

                        }
                    %>

                </div>

            </div>


            <div class="tab-pane fade" id="ios">

            </div>
            <div class="tab-pane fade" id="jmeter">

            </div>
            <div class="tab-pane fade" id="ejb">

            </div>

        </div>


    </div>

</div>
<!--分页	-->
<div>
    <ul class="pagination">
        <%
            int pageAmount;
            String url = request.getScheme() + "://";
            url += request.getHeader("host");
            url += request.getRequestURI();
            if (request.getQueryString() != null)
                url += "?" + request.getQueryString();
            url = url.replace("search.jsp", "SearchForm");

            if (orderInfosBySearch.size() != 0)
                pageAmount = orderInfosBySearch.size() / 6 + (orderInfosBySearch.size() % 6 != 0 ? 1 : 0);
            else
                pageAmount = 1;

            if (pageAmount <= 5) {
        %>

        <%
            if (searchPage == 1) {
        %>
        <li id="pagePrev" class="disabled"><a href="#">&laquo;</a></li>
        <%
        } else {
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            int prevpage = searchPage - 1;
            item[index] = Integer.toString(prevpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pagePrev" class="active"><a href=<%=url%>>&laquo;</a></li>
        <%
            }
        %>

        <%

            if (searchPage == 1) {
        %>
        <li id="pageA" class="disabled"><a href="#" style="background-color: #00adff">1</a></li>
        <%
        } else {
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            item[index] = '1';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageA" class="active"><a href=<%=url%>>1</a></li>
        <%
            }
        %>

        <%
            if (searchPage == 2) {
        %>
        <li id="pageB" class="disabled"><a href="#" name="page" style="background-color: #00adff">2</a></li>
        <%
        } else if (pageAmount < 2) {
        %>
        <li id="pageB" class="disabled"><a href="#" name="page">2</a></li>
        <%
        } else {
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            item[index] = '2';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageB" class="active"><a href=<%=url%>>2</a></li>
        <%
            }
        %>

        <%
            if (searchPage == 3) {
        %>
        <li id="pageC" class="disabled"><a href="#" style="background-color: #00adff">3</a></li>
        <%
        } else if (pageAmount < 3) {
        %>
        <li id="pageC" class="disabled"><a href="#">3</a></li>
        <%
        } else {
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            item[index] = '3';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageC" class="active"><a href=<%=url%>>3</a></li>
        <%
            }
        %>

        <%
            if (searchPage == 4) {
        %>
        <li id="pageD" class="disabled"><a href="#" style="background-color: #00adff">4</a></li>
        <%
        } else if (pageAmount < 4) {
        %>
        <li id="pageD" class="disabled"><a href="#">4</a></li>
        <%
        } else {
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            item[index] = '4';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageD" class="active"><a href=<%=url%>>4</a></li>
        <%
            }
        %>

        <%
            if (searchPage == 5) {
        %>
        <li id="pageE" class="disabled"><a href="#" style="background-color: #00adff">5</a></li>
        <%
        } else if (pageAmount < 5) {
        %>
        <li id="pageE" class="disabled"><a href="#">5</a></li>
        <%
        } else {
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            item[index] = '5';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageE" class="active"><a href=<%=url%>>5</a></li>
        <%
            }
        %>

        <%
            if (searchPage == pageAmount || searchPage == 5) {
        %>
        <li id="pageNext" class="disabled"><a href="#">&raquo;</a></li>
        <%
        } else {
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            int nextpage = searchPage + 1;
            item[index] = Integer.toString(nextpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageNext" class="active"><a href=<%=url%>>&raquo;</a></li>
        <%
            }
        } else {
            if (searchPage == 1) {
        %>
        <li id="pagePrev" class="disabled"><a href="#">&laquo;</a></li>
        <li id="pageA" class="disabled"><a href="#" style="background-color: #00adff">1</a></li>
        <%
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            item[index] = '2';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageB" class="active"><a href=<%=url%>>2</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = '3';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageC" class="active"><a href=<%=url%>>3</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = '4';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageD" class="active"><a href=<%=url%>>4</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageE" class="active"><a href=<%=url%>><%=pageAmount%>...</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            int nextpage = searchPage + 1;
            item[index] = Integer.toString(nextpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageNext" class="active"><a href=<%=url%>>&raquo;</a></li>
        <%
        } else if (searchPage == 2) {
        %>

        <%
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            int prevpage = searchPage - 1;
            item[index] = Integer.toString(prevpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pagePrev" class="active"><a href=<%=url%>>&laquo;</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = '1';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageA" class="active"><a href=<%=url%>>1</a></li>
        <li id="pageB" class="disabled"><a href="#" name="page" style="background-color: #00adff">2</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = '3';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageC" class="active"><a href=<%=url%>>3</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = '4';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageD" class="active"><a href=<%=url%>>4</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageE" class="active"><a href=<%=url%>><%=pageAmount%>...</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            int nextpage = searchPage + 1;
            item[index] = Integer.toString(nextpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageNext" class="active"><a href=<%=url%>>&raquo;</a></li>

        <%
        } else if (searchPage == pageAmount - 1) {
        %>

        <%
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            int prevpage = searchPage - 1;
            item[index] = Integer.toString(prevpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pagePrev" class="active"><a href=<%=url%>>&laquo;</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = '1';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageA" class="active"><a href=<%=url%>>1...</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount - 3).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageB" class="active"><a href=<%=url%>><%=pageAmount - 3%>
        </a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount - 2).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageC" class="active"><a href=<%=url%>><%=pageAmount - 2%>
        </a></li>
        <li id="pageD" class="disabled"><a href="#" style="background-color: #00adff"><%=pageAmount - 1%>
        </a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageE" class="active"><a href=<%=url%>><%=pageAmount%>
        </a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            int nextpage = searchPage + 1;
            item[index] = Integer.toString(nextpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageNext" class="active"><a href=<%=url%>>&raquo;</a></li>

        <%
        } else if (searchPage == pageAmount) {
        %>

        <%
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            int prevpage = searchPage - 1;
            item[index] = Integer.toString(prevpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pagePrev" class="active"><a href=<%=url%>>&laquo;</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = '1';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageA" class="active"><a href=<%=url%>>1...</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount - 3).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageB" class="active"><a href=<%=url%>><%=pageAmount - 3%>
        </a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount - 2).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageC" class="active"><a href=<%=url%>><%=pageAmount - 2%>
        </a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount - 1).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageD" class="active"><a href=<%=url%>><%=pageAmount - 1%>
        </a></li>
        <li id="pageE" class="disabled"><a href="#" style="background-color: #00adff"><%=pageAmount%>
        </a></li>
        <li id="pageNext" class="disabled"><a href="#" name="page">&raquo;</a></li>

        <%
        } else {
        %>

        <%
            int index = url.indexOf("page=") + 5;
            char[] item = url.toCharArray();
            int prevpage = searchPage - 1;
            item[index] = Integer.toString(prevpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pagePrev" class="active"><a href=<%=url%>>&laquo;</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = '1';
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageA" class="active"><a href=<%=url%>>1...</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(searchPage - 1).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageB" class="active"><a href=<%=url%>><%=searchPage - 1%>
        </a></li>

        <li id="pageC" class="disabled"><a href="#" name="page" style="background-color: #00adff"><%=searchPage%>
        </a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(searchPage + 1).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageD" class="active"><a href=<%=url%>><%=searchPage + 1%>
        </a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            item[index] = Integer.toString(pageAmount).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageE" class="active"><a href=<%=url%>><%=pageAmount%>...</a></li>
        <%
            index = url.indexOf("page=") + 5;
            item = url.toCharArray();
            int nextpage = searchPage + 1;
            item[index] = Integer.toString(nextpage).charAt(0);
            url = Arrays.toString(item).replace("[", "");
            url = url.replace("]", "");
            url = url.replace(",", "");
            url = url.replace(" ", "");
        %>
        <li id="pageNext" class="active"><a href=<%=url%>>&raquo;</a></li>
        <%
                }
            }
        %>
    </ul>
</div>

<!--footer-->

<div id="footer">
    <ul id="foot">
        <li><a href="#">关于我们</a>|</li>
        <li><a href="#">合作伙伴</a>|</li>
        <li><a href="#">合作联系</a>|</li>
        <li><a href="#">隐私保护</a>|</li>
        <li><a href="#">投诉建议</a>|</li>
        <li><a href="#">免责声明</a>|</li>
        <li><a href="#">诚聘英才</a></li>
    </ul>
    <p id="foot-1" style="color: #999999;">Copyright © 2002-2019 shuyou.com 版权所有
        <a href="#">ICP证：浙B6-88888888 （金华比奇网络技术有限公司）</a>
        <a href="#">【网络文化经营许可证】浙网文[2016]0188-088号</a>
    </p>
    <!--<div id="foot-2">-->
    <ul id="foot-2-1">
        <li><a href="#">友情合作：</a></li>
        <li><a href="#">178游戏网</a>|</li>
        <li><a href="#">新浪游戏频道</a>|</li>
        <li><a href="#">腾讯游戏频道</a>|</li>
        <li><a href="#">太平洋游戏网</a>|</li>
        <li><a href="#">绿盟软件联盟</a>|</li>
        <li><a href="#">40407网页游戏</a>|</li>
        <li><a href="#">9U9U网页游戏</a>|</li>
        <li><a href="#">人人游戏</a>|</li>
        <li><a href="#">天空下载</a>|</li>
        <li><a href="#">游戏狗</a>|</li>
        <li><a href="#">部落冲突</a>|</li>
        <li><a href="#">2345页游</a>|</li>
        <li><a href="#">手游客栈</a>|</li>
        <li><a href="#">51wan游戏</a>|</li>
        <li><a href="#">贪玩游戏</a>|</li>
        <li><a href="#">602网页游戏</a>|</li>
        <li><a href="#">巴士手机游戏</a>|</li>
        <li><a href="#">ZOL软件下载</a>|</li>
        <li><a href="#">ZOL手机应用</a>|</li>
        <li><a href="#">笨游戏</a>|</li>
        <li><a href="#">17173DNF</a>|</li>
    </ul>
</div>

<script src="plugins/jquery/jquery.min.js" type="text/javascript"></script>
<!--引入bootstrap.min.js文件-->
<script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script>
    $(function () {
        //通用头部搜索切换
        $('#search-hd').find('.search-hovertree-input').on('input propertychange', function () {
            let val = $(this).val();
            if (val.length > 0) {
                $('#search-hd').find('.pholder').hide(0);
            }
            else {
                // language=JQuery-CSS
                let index;
                index = $('#search-bd').find('li.selected').index();
                $('#search-hd').find('.pholder').eq(index).show().siblings('.pholder').hide(0);
            }
        });
        $('#search-bd').find('li').click(function () {
            let index = $(this).index();
            let search_hd = $('#search-hd');
            search_hd.find('.pholder').eq(index).show().siblings('.pholder').hide(0);
            search_hd.find('.search-hover' + 'tree-input').eq(index).show().siblings('.search-hovertree-input').hide(0);
            $(this).addClass('selected').siblings().removeClass('selected');
            search_hd.find('.search-hovertree-input').val('');
        });
    });
    //					显示更多
    $(document).ready(function () {
        $(".myBtn").click(function () {
            $(".ss").toggle();
        });
    });

    $(document).ready(function () {
        $(".myBtn-1").click(function () {
            $(".ss-1").toggle();
        });
    });

    //						切换更多
    function myFunction() {
        let x = document.getElementsByClassName("myBtn");
        x = x.item(0);
        let y = "精简显示";
        if (x.innerHTML === "显示全部") {
            x.innerHTML = y;
        }
        else {
            x.innerHTML = "显示全部";
        }
    }

    function myFunction1() {
        let x = document.getElementsByClassName("myBtn-1");
        x = x.item(0);
        let y = "精简显示";
        if (x.innerHTML === "显示全部") {
            x.innerHTML = y;
        }
        else {
            x.innerHTML = "显示全部";
        }
    }
</script>
</body>
</html>
