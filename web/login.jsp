<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/5 0005
  Time: 下午 3:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>舒游用户登录</title>
    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/plugins/bootstrap/css/bootstrap.min.css"/>
    <style type="text/css">
        #first {
            width: 1076.2px;
            margin: auto;
        }

        .navbar-inverse {
            background-color: white;
            border-color: #080808;
            margin: auto;
        }

        .navbar {
            position: relative;
            min-height: 50px;
            margin-bottom: 20px;
            border: 1px;
        }

        .navbar-inverse .navbar-nav > li > a {
            color: black;
            font-size: 12px;
        }

        #middleDiv {
            border: 1px solid #B56A42;
            width: auto;
            height: 600px;
        }

        .carousel-inner > .item > a > img, .carousel-inner > .item > img .thumbnail a > img, .thumbnail > img {
            width: 758px;
            height: auto;
            display: inline;
        }

        #ziol {
            margin-right: 0;
        }

        .myLogin {
            width: 10%;
            margin-left: 841px;
            margin-right: 242px;
            padding: 30px 50px;
            border-radius: 5px;
            /**边框的阴影**/
            box-shadow: 0 0 4px #ccc;
        }

        .userExit {
            color: white;
            /**设置文字背影**/
            text-shadow: 0 2px 2px #000;
        }

        #abc > input {
            width: 265px;
        }

        #efg > input {
            width: 265px;
        }

        #bod {
            width: 500px;
        }

        .btn-default {
            color: ghostwhite;
            background-color: orangered;
            border-color: #ccc;
        }

        .form-horizontal .form-group {
            margin-right: 15px;
            margin-left: 15px;
        }

        #middleDivLeft {
            background-image: none;
        }

        #middleDivRight {
            background-image: none;
        }

        .carousel-fade .carousel-inner .item {
            opacity: 0;
            -webkit-transition-property: opacity;
            -moz-transition-property: opacity;
            -o-transition-property: opacity;
            transition-property: opacity;
        }

        .carousel-fade .carousel-inner .active {
            opacity: 1;
        }

        #btnLogin {
            margin-left: 82px;
            background-color: red;
        }

        #footer {
            width: 1000px;
            height: 176px;
            margin: auto;
        }

        #foot {
            width: 600px;
            height: 20px;
            margin: auto;
            margin-top: 30px;
        }

        #foot li {
            width: 66.4px;
            height: 18px;
            color: #DDDDDD;
            list-style: none;
            float: left;
            font-size: 12px;
        }

        #foot a {
            width: 48px;
            height: 14.4px;
            margin-right: 8px;
            color: #999999;
        }

        #foot-1 {
            width: 1000px;
            height: 48px;
            padding-top: 15px;
            padding-bottom: 15px;
            margin-left: 60px;
        }

        #foot-1 a {
            margin-left: 10px;
            color: #999999;
            font-size: 12px;
        }

        #foot-2-1 {
            width: 1000px;
            height: 36px;
        }

        #foot-2-1 li {
            float: left;
            color: #DDDDDD;
            list-style: none;
            font-size: 12px;
        }

        #foot-2-1 a {
            margin-left: 8px;
            margin-right: 8px;
            color: #999999;
        }

        .navbar-inverse .navbar-nav > li > a:focus, .navbar-inverse .navbar-nav > li > a:hover {
            color: red;
            background-color: transparent;
        }
    </style>
</head>

<body>
<nav>
    <div class="container-fluid">
        <div class="row">
            <div id="first">
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target="#example-navbar-collapse">
                                <span class="sr-only">切换导航</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <a href="..//"><img src="img/logo_140.png"/></a>

                        </div>
                        <div class="collapse navbar-collapse" id="example-navbar-collapse">
                            <ul class="nav navbar-nav navbar-right mya">
                                <li>
                                    <a href="javascript:void(0);">您好，尊敬的用户</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">注册</a>
                                </li>

                                <li>
                                    <a href="javascript:void(0);">客服中心</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" style="padding-right: 0">用户帮助中心</a>
                                </li>
                                <li>
                                    <img src="img/img_login.png"/>
                                </li>
                            </ul>

                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</nav>

<!--导航...-->
<div id="middleDiv" style="background-color: #B56A42">
    <div class="middle pull-left carousel-fade" data-interval="false" style="margin: 50px 50px;">
        <div id="myCarousel" class="carousel slide margin-right: 0; margin-left: 0;">
            <!-- 轮播（Carousel）指标 -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel carousel-fade" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel carousel-fade" data-slide-to="1"></li>
                <li data-target="#myCarousel carousel-fade" data-slide-to="2"></li>
                <li data-target="#myCarousel carousel-fade" data-slide-to="3"></li>
            </ol>
            <!-- 轮播（Carousel）项目 -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="img/dla1.png" alt="First slide">
                </div>
                <div class="item">
                    <img src="img/dla2.png" alt="Second slide">
                </div>
                <div class="item">
                    <img src="img/dla3.png" alt="Third slide">
                </div>
                <div class="item">
                    <img src="img/dla4.png" alt="Fourth slide">
                </div>
            </div>
            <!-- 轮播（Carousel）导航 -->
            <a id="middleDivLeft" class="left carousel-control carousel-fade" href="#myCarousel" role="button"
               data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a id="middleDivRight" class="right carousel-control carousel-fade" href="#myCarousel" role="button"
               data-slide="next">
                <span id="ziol" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <div id="bod" class="myLogin" style="margin-top: 132px; background-color: white">
        <form class="form-horizontal" role="form">
            <fieldset>
                <legend><h2 class="userExit">用户登录入口</h2></legend>
                <div class="form-group has-feedback">
                    <label for="firstname" class="col-sm-2 control-label">用户</label>
                    <div id="abc" class="col-md-4">
                        <input type="text" class="form-control" id="firstname" placeholder="用户名/手机号码">
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label for="lastname" class="col-sm-2 control-label">密码</label>
                    <div id="efg" class="col-md-4">
                        <input type="password" class="form-control" id="lastname" placeholder="请输入您的密码">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">请记住我

                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button id="btnLogin" type="submit" class="btn btn-default">登录</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>


<!--footer-->
<div id="footer">
    <ul id="foot">
        <li><a href="javascript:void(0);">关于我们</a>|</li>
        <li><a href="javascript:void(0);">合作伙伴</a>|</li>
        <li><a href="javascript:void(0);">合作联系</a>|</li>
        <li><a href="javascript:void(0);">隐私保护</a>|</li>
        <li><a href="javascript:void(0);">投诉建议</a>|</li>
        <li><a href="javascript:void(0);">免责声明</a>|</li>
        <li><a href="javascript:void(0);">诚聘英才</a></li>
    </ul>
    <p id="foot-1" style="color: #999999; font-size: 12px;">Copyright © 2002-2019 shuyou.com 版权所有
        <a href="javascript:void(0);">ICP证：浙B6-88888888 （金华比奇网络技术有限公司）</a>
        <a href="javascript:void(0);">【网络文化经营许可证】浙网文[2016]0188-088号</a>
    </p>

    <ul id="foot-2-1" style="padding-left: 0;">
        <li><a href="javascript:void(0);">友情合作：</a></li>
        <li><a href="javascript:void(0);">178游戏网</a>|</li>
        <li><a href="javascript:void(0);">新浪游戏频道</a>|</li>
        <li><a href="javascript:void(0);">腾讯游戏频道</a>|</li>
        <li><a href="javascript:void(0);">太平洋游戏网</a>|</li>
        <li><a href="javascript:void(0);">绿盟软件联盟</a>|</li>
        <li><a href="javascript:void(0);">40407网页游戏</a>|</li>
        <li><a href="javascript:void(0);">9U9U网页游戏</a>|</li>
        <li><a href="javascript:void(0);">人人游戏</a>|</li>
        <li><a href="javascript:void(0);">天空下载</a>|</li>
        <li><a href="javascript:void(0);">游戏狗</a>|</li>
        <li><a href="javascript:void(0);">部落冲突</a>|</li>
        <li><a href="javascript:void(0);">2345页游</a>|</li>
        <li><a href="javascript:void(0);">手游客栈</a>|</li>
        <li><a href="javascript:void(0);">51wan游戏</a>|</li>
        <li><a href="javascript:void(0);">贪玩游戏</a>|</li>
        <li><a href="javascript:void(0);">602网页游戏</a>|</li>
        <li><a href="javascript:void(0);">巴士手机游戏</a>|</li>
        <li><a href="javascript:void(0);">ZOL软件下载</a>|</li>
        <li><a href="javascript:void(0);">ZOL手机应用</a>|</li>
        <li><a href="javascript:void(0);">笨游戏</a>|</li>
        <li><a href="javascript:void(0);">17173DNF</a>|</li>
    </ul>
</div>
<script src="<%=request.getContextPath()%>/plugins/jquery/jquery.min.js" type="text/javascript"
        charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/plugins/bootstrap/js/bootstrap.js" type="text/javascript"
        charset="utf-8"></script>
<script>
    let myCarousel = $('#myCarousel');
    myCarousel.carousel({
        interval: 5000
    });
    myCarousel.on('slide.bs.carousel', function (event) {
        let $hoder = $('#myCarousel').find('.item'),
            $items = $(event.relatedTarget);
        //getIndex就是轮播到当前位置的索引
        let getIndex = $hoder.index($items);

        if (getIndex === 0) {
            document.getElementById("middleDiv").style.backgroundColor = "#B56A42";
            document.getElementById("middleDiv").style.border = "1px solid #B56A42";
        }
        else if (getIndex === 1) {
            document.getElementById("middleDiv").style.backgroundColor = "#25201C";
            document.getElementById("middleDiv").style.border = "1px solid #25201C";
        }
        else if (getIndex === 2) {
            document.getElementById("middleDiv").style.backgroundColor = "#232d32";
            document.getElementById("middleDiv").style.border = "1px solid #232d32";
        }
        else {
            document.getElementById("middleDiv").style.backgroundColor = "#FEA909";
            document.getElementById("middleDiv").style.border = "1px solid #FEA909";
        }
    });
</script>
</body>
</html>