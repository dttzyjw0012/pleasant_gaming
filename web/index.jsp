<%@ page import="com.comfortable_game.entity.GameType" %>
<%@ page import="java.util.List" %>
<%@ page import="com.comfortable_game.entity.Game" %>
<%@ page import="com.comfortable_game.controller.GetTypeById" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/5 0005
  Time: 下午 3:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>舒游游戏网</title>
    <!--引入bootstrap.min.css文件-->
    <link rel="stylesheet" type="text/css" href="plugins/bootstrap/css/bootstrap.css"/>
    <link href="css/base.css" type="text/css" rel="stylesheet"/>
    <style type="text/css">
        #display:hover ul {
            display: block;
            float: left;
        }

        #top {
            padding-right: 250px;
            padding-left: 250px;
            margin-right: auto;
            margin-left: auto;
        }

        #top-left a {
            height: 50px;
            font-size: 12px;
            line-height: 20px;
            padding: 1px 1px 1px 12px;
        }

        .top-all {
            position: relative;
            height: 25px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            color: #777;
        }

        #maxtop a {
            padding-top: 0;
            padding-bottom: 0;
            padding-left: 1px;
        }

        .top-right {
            padding-right: 10px;
        }

        #dropdown-font a {
            font-size: 13px;
            padding-left: 10px;
        }

        .search-hovertree-form {
            width: 575px;
            height: 123px;
            overflow: hidden;
        }

        .search-hovertree-form .search-bd {
            height: 25px;
        }

        .search-hovertree-form .search-bd li {
            font-size: 12px;
            width: 60px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            float: left;
            cursor: pointer;
            background-color: #eee;
            color: #666;
        }

        .search-hovertree-form .search-bd li.selected {
            color: #fff;
            font-weight: 700;
            background-color: #3983E8;
        }

        .search-hovertree-form .search-hd {
            height: 34px;
            background-color: #3983E8;
            padding: 3px;
            position: relative;
        }

        .search-hovertree-form .search-hd .search-hovertree-input {
            width: 424px;
            height: 22px;
            line-height: 22px;
            padding: 6px 0;
            background: none;
            text-indent: 10px;
            border: 0;
            outline: none;
            position: relative;
            left: 3px;
            top: 2px;
            z-index: 5;
            #margin-left: -10px;
        }

        .search-hovertree-form .search-hd .btn-search {
            width: 70px;
            height: 34px;
            line-height: 34px;
            position: absolute;
            right: 3px;
            top: 1px;
            border: 0;
            z-index: 6;
            cursor: pointer;
            font-size: 13px;
            color: #fff;
            font-weight: 700;
            background: none;
            outline: none;
        }

        .search-hovertree-form .search-hd .pholder {
            display: inline-block;
            font-size: 12px;
            color: #999;
            position: absolute;
            left: 13px;
            top: 11px;
            z-index: 4;
            padding: 2px 0 2px 25px;
            top: 11px;
        }

        .search-hovertree-form .search-bg {
            width: 350px;
            height: 28px;
            background-color: #fff;
            position: absolute;
            left: 150px;
            top: 3px;
            z-index: 1;
            float: left;
        }

        #photo {
            height: 70px;
            width: 315px;
            float: left;
        }

        .choose {
            width: 70px;
            height: 28px;
            float: left;
        }

        #navigator-style {
            width: 1000px;
            height: 34px;
            margin: auto;
        }

        #navigator-style li {
            width: 106px;
            height: 34px;
            font-family: arial, fantasy;
            float: left;
            margin: auto;
            line-height: 34px;

        }

        #navigator-background {
            width: 100%;
            height: 34px;
            background: url(img/header_repeat.png);
        }

        #navigator-style a {
            color: white;
            font-weight: 700;
            font-size: 14px;
        }

        #maxouter {
            margin: auto;
            width: 1020px;
            height: 1040px;
        }

        #left {
            width: 730px;
            height: 1030px;
            float: left;
        }

        #carousel-outer {
            width: 730px;
            height: 325px;
            margin-top: 15px;
        }

        #tab {
            width: 730px;
            height: 646px;
            margin-top: 15px;
            border: 1px solid #CCCCCC;

        }

        #tab-1 li {
            width: 109px;
            height: 38px;
            font-size: 14px;
        }

        #tab-1 {
            width: 728.6px;
            height: 39px;
            background-color: #fbfbfb;
            border-bottom: 1.6px solid blue;
        }

        #tab-1 a {
            padding-bottom: 7px;
            padding-left: 24px;
            border-radius: 0 0 0 0;
        }

        #tab-1 a:hover {
            color: blue;
            background-color: #fbfbfb;
            border: 0;

        }

        .list-1 {
            width: 735px;
            height: 540px;
        }

        .list-1 li a {
            width: 145px;
            height: 130px;
            float: left;
        }

        .list-1 img {
            margin: 20px auto auto;
            width: 90px;
            height: 90px;
            border-radius: 10px;
        }

        .list-1 p {
            width: 145px;
            height: 26px;
        }

        #right {
            width: 270px;
            height: 1030px;
            margin-top: 15px;
            margin-left: 9px;
            float: left;
        }

        #right-top {
            width: 258px;
            height: 37px;
        }

        #right-top li {
            width: 128px;
            height: 37px;
            float: left;
        }

        #right-top2 {
            width: 262px;
            height: 288px;
        }

        #right-ul li {
            width: 65px;
            height: 96px;
            float: left;
        }

        #right-ul a {
            width: 64.6px;
            height: 96.6px;
            border: 1px solid #F7F7F7;
            float: left;
        }

        #right-ul img {
            width: 63px;
            height: 64px;
        }

        #right-top3 {
            border: 1px solid #D4D4D4;
            width: 258px;
            height: 164.8px;
            margin-top: 10px;
        }

        .right-top3-ul {
            width: 68.5px;
            height: 30.8px;
        }

        #right-gonggao {
            width: 42px;
            height: 30.8px;
            padding-left: 0;
            padding-right: 0;
        }

        .right-top3-list {
            width: 258px;
            height: 110px;
        }

        .right-top3-list-1 li {
            width: 258px;
            height: 20px;
            text-align: left;
            text-indent: 23px;
            margin-top: 10px;
        }

        .right-top3-list-2 li {
            width: 129px;
            height: 20px;
            text-indent: 23px;
            margin-top: 5px;
            text-align: left;
            float: left;
        }

        .right-top3-list-3 {
            width: 258px;
            height: 66px;
        }

        #tiwen {
            width: 228px;
            height: 29.8px;
        }

        #right-4 {
            width: 258px;
            height: 127.6px;
            border: 1px solid #D4D4D4;
            margin-top: 15px;
        }

        #right-4 ul {
            width: 258px;
            height: 30.8px;
        }

        .right-4-li a {
            width: 84px;
            height: 31px;

        }

        .QQbtn-a {
            width: 55px;
            height: 23px;
            background-color: #4c8bf8;
            color: white;
        }

        .QQbtn li {
            width: 220px;
            height: 45px;
            padding-top: 15px;
        }

        .right-4-L {
            float: left;
            margin-left: 30px;
        }

        .right-4-R {
            float: right;

        }

        #right-5 {
            width: 258px;
            height: 320px;
            border: 1px solid #D4D4D4;
        }

        #right-5-1 {
            width: 258px;
            height: 28px;
            border-bottom: 1px solid #D4D4D4;
            text-align: left;
            padding-left: 10px;

        }

        #right-5-2 {
            width: 258px;
            height: 269px;
        }

        #right-5-2-li li {
            width: 238px;
            height: 55px;
            padding-top: 8px;
            padding-bottom: 8px;
            margin-left: 15px;
        }

        #right-5-2-li {
            margin-top: 10px;
            text-align: left;
        }

        hr {
            margin-top: 0;
            margin-bottom: 0;
        }

        #but {
            width: 253px;
            height: 25px;
            margin-top: 5px;
            margin-left: 5px;
        }

        .but-style {
            width: 69px;
            height: 25px;
            font-size: 12px;
            text-align: center;
            padding-top: 3px;
        }

        #top-1 {
            width: 1000px;
            height: 123px;
            padding-top: 15px;
            margin: auto;
        }

        #footer {
            width: 1000px;
            height: 176px;
            margin: auto;
        }

        #foot {
            width: 485px;
            height: 18px;
            margin: auto;
        }

        #foot li {
            width: 66.4px;
            height: 18px;
            color: #DDDDDD;
            float: left;
        }

        #foot a {
            width: 48px;
            height: 14.4px;
            margin-right: 8px;
            color: #999999;
        }

        #foot-1 {
            width: 1000px;
            height: 48px;
            padding-top: 15px;
            padding-bottom: 15px;
        }

        #foot-1 a {
            margin-left: 10px;
            color: #999999;
        }

        #foot-2-1 {
            width: 1000px;
            height: 36px;
        }

        #foot-2-1 li {
            float: left;
            color: #DDDDDD;
        }

        #foot-2-1 a {
            margin-left: 8px;
            margin-right: 8px;
            color: #999999;
        }
    </style>

</head>
<body>

<!--导航栏-->
<div class="cantainer-fluid"></div>
<div class="row"></div>
<!--导航栏-->
<nav class="top-all navbar-default" role="navigation" style="margin-bottom: 0;">
    <div id="top">
        <div class="navbar-header" id="top-left">
            <span>欢迎到舒游网络</span><a href="login.jsp">请登陆</a>
            <a href="javascript:void(0);">免费注册</a>
        </div>
        <ul class="nav navbar-nav navbar-right" id="maxtop">
            <li class="active">
                <a href="pro_info.jsp" style="padding-left: 15px;padding-top: 2px;">我的舒游</a>
            </li>
            <li>
                <a href="javascript:void(0);"><span class="top-right">|</span>金币兑换</a>
            </li>
            <li>
                <a href="javascript:void(0);"><span class="top-right">|</span>收藏夹</a>
            </li>
            <li>
                <a href="javascript:void(0);"><span class="top-right">|</span>客服中心</a>
            </li>
            <li>
                <a href="javascript:void(0);"><span class="top-right">|</span><img src="img/手机图标.png"/>&nbsp;移动版</a>
            </li>
            <li id="display" class="nav navbar-nav navbar-right">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="top-right">|</span>网站导航 <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" id="dropdown-font">
                    <li class="dropdown-header">商品类</li>
                    <li>
                        <a href="coin_trade.jsp">金币交易</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">装备交易</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">账号交易</a>
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">服务类
                    </li>
                    <li>
                        <a href="javascript:void(0);">资料修改</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">安全中心</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">推广联盟</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<!--搜索栏-->
<div id="top-1">
    <div id="photo">
        <img src="img/logo_140.png" style="padding-top: 16px;"/>
    </div>
    <div class="search-hovertree-form">
        <div id="search-bd" class="search-bd">
            <ul style="background-color: white; margin-bottom: 10px">
                <li class="selected">普通搜索</li>
                <li>简单搜索</li>
            </ul>
        </div>
        <div id="search-hd" class="search-hd">
            <form action="SearchForm" method="GET">
                <select class="choose" style="margin-right: 3px; font-size: 11px" name="gameType" title="游戏名称">
                    <option value="">游戏名称</option>
                    <option value="">毒奶粉</option>
                    <option value="">英雄联盟</option>
                    <option value="">绝地求生</option>
                    <option value="">穿越火线</option>
                    <option value="">天龙八部</option>
                    <option value="">魔兽世界</option>
                </select>
                <select class="choose" style="font-size: 11px" name="tradeType" title="游戏分类">
                    <option value="">游戏分类</option>
                    <option value="">账号充值</option>
                    <option value="">游戏代练</option>
                    <option value="">账号租赁</option>
                    <option value="">装备</option>
                    <option value="">游戏币</option>
                    <option value="">激活码/CDK</option>
                </select>
                <div class="search-bg"></div>
                <input type="text" id="s1" class="search-hovertree-input" placeholder="请输入任意关键字" name="searchKey">
                <input type="hidden" name="page" value="1">
                <button id="submit" class="btn-search" value="搜索" type="submit">搜索</button>
            </form>
        </div>
    </div>
</div>
<!--<导航栏2-->
<div id="navigator-background">
    <ul id="navigator-style">
        <li style="padding-left: 30px;"><a href="..//">首页</a></li>
        <li><a href="coin_trade.jsp">金币交易</a></li>
        <li><a href="javascript:void(0);">账号交易</a></li>
        <li><a href="javascript:void(0);">装备交易</a></li>
        <li><a href="javascript:void(0);">手游交易</a></li>
        <li><a href="javascript:void(0);">游戏代练</a></li>
        <li><a href="javascript:void(0);">点卡交易</a></li>
        <li><a href="javascript:void(0);">账号租赁</a></li>
    </ul>
</div>

<div id="maxouter">
    <div id="left">
        <!--轮播-->
        <div id="carousel-outer">

            <div id="myCarousel" class="carousel slide">

                <!-- 轮播（Carousel）指标 -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <!-- 轮播（Carousel）项目 -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="img/4.png" alt="First slide">
                    </div>
                    <div class="item">
                        <img src="img/3.png" alt="Second slide">
                    </div>
                    <div class="item">
                        <img src="img/5.png" alt="Third slide">
                    </div>
                </div>
                <!-- 轮播（Carousel）导航 -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
        <%!
            @SuppressWarnings("unchecked")
            private static <T> T castAttribute(Object obj) {
                return (T) obj;
            }
        %>
        <%
            List<GameType> gameTypelist = castAttribute(request.getAttribute("gameTypelist"));
            List<Game> gameListByHot = castAttribute(request.getAttribute("gameListHot"));
            GetTypeById getTypeById = (GetTypeById) request.getAttribute("getTypeById");
            List<Game> gameListTypeZero = castAttribute(request.getAttribute("gameListTypeZero"));
        %>
        <!--标签-->
        <div id="tab">
            <ul id="tab-1" class="nav nav-tabs">
                <li class="active">
                    <a href="#home" data-toggle="tab">
                        热门游戏
                    </a>
                </li>
                <li>
                    <a href="#pc" data-toggle="tab">
                        <%=getTypeById.getTypeById(gameTypelist.get(0).getId())%>
                    </a>
                </li>
                <li>
                    <a href="#phone" data-toggle="tab">
                        <%=getTypeById.getTypeById(gameTypelist.get(1).getId())%>
                    </a>
                </li>
                <li>
                    <a href="#1" data-toggle="tab">
                        <%=getTypeById.getTypeById(gameTypelist.get(2).getId())%>
                    </a>
                </li>
                <li>
                    <a href="#2" data-toggle="tab">
                        <%=getTypeById.getTypeById(gameTypelist.get(3).getId())%>
                    </a>
                </li>
                <li>
                    <a href="#3" data-toggle="tab" style="padding-right: 14px">
                        <%=getTypeById.getTypeById(gameTypelist.get(4).getId())%>
                    </a>
                </li>
            </ul>
            <% int indexHot = 0; %>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="home">
                    <ul class="list-1">
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/QQ截图20180902223310.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-6.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-5.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-4.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/666.jpg"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-7.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-13.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-22.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-15.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/2806.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-11.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-10.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-12.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-8.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-14.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-20.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-19.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-18.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-21.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                            <% indexHot++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-17.png"/>
                                <p><%=gameListByHot.get(indexHot).getGameName()%>
                                </p></a>
                        </li>
                    </ul>
                </div>
                <%
                    int indexTypeOne = 0;
                %>
                <div class="tab-pane fade" id="pc">
                    <ul class="list-1">
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/QQ截图20180902223310.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-6.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-5.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-4.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/666.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-12.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-13.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/sy177.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/nbs.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/470.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-11.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/未标题-22.png"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/691.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/628.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/610.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/593.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/585.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/579.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/455.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                            <% indexTypeOne++; %>
                        </li>
                        <li>
                            <a onmouseover="mouseOver(this);" onmouseout="mouseOff(this);"
                               href="javascript:void(0);"><img
                                    src="img/001.jpg"/>
                                <p><%=gameListTypeZero.get(indexTypeOne).getGameName()%>
                                </p></a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="phone">
                    <a><img src="img/QQ截图20180902190037.png"/></a>
                    <p>待开发。</p>
                </div>
                <div class="tab-pane fade" id="1">
                    <p>待开发。</p>
                </div>
                <div class="tab-pane fade" id="2">
                    <p>待开发。</p>
                </div>
                <div class="tab-pane fade" id="3">
                    <p>待开发。</p>
                </div>
            </div>
            <div>
                <a type="button" class="btn btn-default" href="javascript:void(0);">查看更多游戏</a>
            </div>
        </div>
    </div>

    <div id="right">
        <!--我要买 /卖-->
        <div>
            <ul id="right-top">
                <li><a href="javascript:void(0);"><img src="img/buy.png"/></a></li>
                <li style="padding-left: 0.8px;"><a href="javascript:void(0);"><img src="img/未标题-3.png"/></a></li>
            </ul>
        </div>
        <!--游戏币 点卡 装备-->
        <div id="right-top2">
            <ul id="right-ul">
                <li><a href="javascript:void(0);"><img src="img/未.png">
                    <p>游戏币</p></a></li>
                <li><a href="javascript:void(0);"><img src="img/题-2.png">
                    <p>QQ</p><</a>/li>
                <li><a href="javascript:void(0);"><img src="img/未-3.png">
                    <p>点卡</p></a></li>
                <li><a href="javascript:void(0);"><img src="img/题-4.png">
                    <p>装备</p></a></li>
                <li><a href="javascript:void(0);"><img src="img/bm_sycz.png">
                    <p>手游充值</p></a></li>
                <li><a href="javascript:void(0);"><img src="img/bm_kfyz.png">
                    <p>官服验证</p></a></li>
                <li><a href="javascript:void(0);"><img src="img/bm_gzh.png">
                    <p>公众号</p></a></li>
                <li><a href="javascript:void(0);"><img src="img/bm_app.png">
                    <p>手机APP</p></a></li>
            </ul>
            <img src="img/15-1.jpg"/>
        </div>
        <!--右侧导航-->
        <div id="right-top3">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a style="padding-left: 0;padding-right: 0;" id="right-gonggao" href="#gonggao"
                       data-toggle="tab">公告</a>
                </li>
                <li>
                    <a style="padding-left: 0;padding-right: 0;" class="right-top3-ul" href="#anquan"
                       data-toggle="tab">交易安全</a>
                </li>
                <li>
                    <a style="padding-left: 0;padding-right: 0;" class="right-top3-ul" href="#zhinan"
                       data-toggle="tab">新手指南</a>
                </li>
                <li>
                    <a style="padding-left: 0;padding-right: 0;" class="right-top3-ul" href="#wenti"
                       data-toggle="tab">常见问题</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="right-top3-list tab-pane fade in active" id="gonggao">
                    <ul class="right-top3-list-1" style="padding-top: 6px">
                        <li>
                            <a href="javascript:void(0);">古剑奇谭网络版免佣公告</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">关于中国网络游戏服务网</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">御龙在天发生服务变更</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">堡垒之夜免佣公告</a>
                        </li>
                    </ul>
                </div>
                <div class="right-top3-list tab-pane fade" id="anquan">
                    <ul class="right-top3-list-2">
                        <li>
                            <a href="javascript:void(0);">交易安全验证</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">异常状况处理</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">舒游账号安全</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">钓鱼网站盗号</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">安全意识</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">假客服欺诈</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">邮件假链接</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">骗术大揭秘</a>
                        </li>
                    </ul>
                </div>
                <div class="right-top3-list tab-pane fade" id="zhinan">
                    <ul class="right-top3-list-2">
                        <li>
                            <a href="javascript:void(0);">交易服务收费标准</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">延迟到帐帮助</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">商品找回帮助</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">交易流程</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">网上银行充值</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">忘记用户名、登陆</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">发布单审核失败</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">订单取消原因</a>
                        </li>
                    </ul>
                </div>
                <div class="right-top3-list tab-pane fade" id="wenti" style="padding-top: 10px">
                    <ul class="right-top3-list-3 right-top3-list-2">
                        <li>
                            <a href="javascript:void(0);">寄售交易注意事项</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">手机如何绑定账户</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">怎样挑选安全账号</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">提现到账时间</a>
                        </li>
                    </ul>
                    <div id="tiwen">
                        <a href="javascript:void(0);">我要提问</a> <span>将在十分钟内回复你的信息</span>
                    </div>

                </div>
            </div>
        </div>

        <!--验证-->
        <div id="right-4">
            <ul class="nav nav-tabs right-4-li">
                <li class="active ">
                    <a style="padding-left: 0;padding-right: 0;" class="right-4-li" href="#QQ"
                       data-toggle="tab">QQ官服验证</a>
                </li>
                <li>
                    <a style="padding-left: 0;padding-right: 0;" class="right-4-li" href="#weixin"
                       data-toggle="tab">微信官服验证</a>
                </li>
                <li>
                    <a style="padding-left: 0;padding-right: 0;" class="right-4-li" href="#wangzhi"
                       data-toggle="tab">网址验证</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="right-top3-list tab-pane fade in active" id="QQ">
                    <ul class="QQbtn">
                        <li><input placeholder="请输入客服QQ号"/><a style="padding-bottom: 4px;padding-top: 1px;"
                                                              class="QQbtn-a" href="javascript:void(0);">验证</a></li>
                        <li><a class="right-4-L" style="color: #999999;" href="javascript:void(0);">识别假客服</a><a
                                class="right-4-R"
                                style="color: #999999;"
                                href="javascript:void(0);">防范钓鱼骗术</a></li>
                    </ul>
                </div>
                <div class="right-top3-list tab-pane fade" id="weixin">
                    <ul class="QQbtn">
                        <li><input placeholder="请输入微信号"/><a style="padding-bottom: 4px;padding-top: 1px;"
                                                            class="QQbtn-a" href="javascript:void(0);">验证</a></li>
                        <li><a class="right-4-L" style="color: #999999;" href="javascript:void(0);">识别假客服</a><a
                                class="right-4-R"
                                style="color: #999999;"
                                href="javascript:void(0);">防范钓鱼骗术</a></li>
                    </ul>
                </div>
                <div class="right-top3-list tab-pane fade" id="wangzhi">
                    <ul class="QQbtn">
                        <li><input placeholder="请输入网址"/><a style="padding-bottom: 4px;padding-top: 1px;" class="QQbtn-a"
                                                           href="javascript:void(0);">验证</a></li>
                        <li style="text-align: left; color: #999999; margin-left: 20px;">可为您验证是否为舒游官方网址或舒游官方合作网址。
                        </li>
                    </ul>
                </div>
            </div>
            <!--咨询-->
            <div id="right-5">
                <div id="right-5-1">
                    <h6><b>咨询/建议/投诉</b></h6>
                </div>
                <div id="right-5-2">
                    <ul id="right-5-2-li">
                        <li>
                            <a href="javascript:void(0);"><span>[账户问题]自己的实名问题可以修改吗？</span></a><br/>
                            <a style="color: #666;"
                               href="javascript:void(0);"><span>我于2018年9月16日 14：59：10拨打...</span></a>
                        </li>
                        <hr/>
                        <li>
                            <a href="javascript:void(0);"><span>[账户交易]服务人员太忙，无法跟我</span></a><br/>
                            <a style="color: #666;"
                               href="javascript:void(0);"><span>我于2018年9月16日 14：59：10拨打...</span></a>
                        </li>
                        <hr/>
                        <li>
                            <a href="javascript:void(0);"><span>[账户问题]资金无法提示</span></a><br/>
                            <a style="color: #666;"
                               href="javascript:void(0);"><span>我于2018年9月16日 14：59：10拨打...</span></a>
                        </li>
                        <hr/>
                        <li>
                            <a href="javascript:void(0);"><span>[账户交易]自己的实名问题可以修改吗？</span></a><br/>
                            <a style="color: #666;"
                               href="javascript:void(0);"><span>我于2018年9月16日 14：59：10拨打...</span></a>
                        </li>
                        <hr/>
                        <li>
                            <div id="but">
                                <a class="btn btn-info but-style">我要咨询</a>
                                <a class="btn btn-warning but-style">我要建议</a>
                                <a class="btn btn-success but-style">我要投诉</a>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!--footer-->
</div>
<div id="footer">
    <ul id="foot">
        <li><a href="javascript:void(0);">关于我们</a>|</li>
        <li><a href="javascript:void(0);">合作伙伴</a>|</li>
        <li><a href="javascript:void(0);">合作联系</a>|</li>
        <li><a href="javascript:void(0);">隐私保护</a>|</li>
        <li><a href="javascript:void(0);">投诉建议</a>|</li>
        <li><a href="javascript:void(0);">免责声明</a>|</li>
        <li><a href="javascript:void(0);">诚聘英才</a></li>
    </ul>
    <p id="foot-1" style="color: #999999;">Copyright © 2002-2019 shuyou.com 版权所有
        <a href="javascript:void(0);">ICP证：浙B6-88888888 （金华比奇网络技术有限公司）</a>
        <a href="javascript:void(0);">【网络文化经营许可证】浙网文[2016]0188-088号</a>
    </p>
    <!--<div id="foot-2">-->
    <ul id="foot-2-1">
        <li><a href="javascript:void(0);">友情合作：</a></li>
        <li><a href="javascript:void(0);">178游戏网</a>|</li>
        <li><a href="javascript:void(0);">新浪游戏频道</a>|</li>
        <li><a href="javascript:void(0);">腾讯游戏频道</a>|</li>
        <li><a href="javascript:void(0);">太平洋游戏网</a>|</li>
        <li><a href="javascript:void(0);">绿盟软件联盟</a>|</li>
        <li><a href="javascript:void(0);">40407网页游戏</a>|</li>
        <li><a href="javascript:void(0);">9U9U网页游戏</a>|</li>
        <li><a href="javascript:void(0);">人人游戏</a>|</li>
        <li><a href="javascript:void(0);">天空下载</a>|</li>
        <li><a href="javascript:void(0);">游戏狗</a>|</li>
        <li><a href="javascript:void(0);">部落冲突</a>|</li>
        <li><a href="javascript:void(0);">2345页游</a>|</li>
        <li><a href="javascript:void(0);">手游客栈</a>|</li>
        <li><a href="javascript:void(0);">51wan游戏</a>|</li>
        <li><a href="javascript:void(0);">贪玩游戏</a>|</li>
        <li><a href="javascript:void(0);">602网页游戏</a>|</li>
        <li><a href="javascript:void(0);">巴士手机游戏</a>|</li>
        <li><a href="javascript:void(0);">ZOL软件下载</a>|</li>
        <li><a href="javascript:void(0);">ZOL手机应用</a>|</li>
        <li><a href="javascript:void(0);">笨游戏</a>|</li>
        <li><a href="javascript:void(0);">17173DNF</a>|</li>
    </ul>
</div>
<!--广告-->
<!--<div id="ad"><img src="img/guanggao.jpg"></div>-->

<!--引入jquery.min.js文件-->
<script src="plugins/jquery/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">$(function () {
    //通用头部搜索切换
    $('#search-hd').find('.search-hovertree-input').on('input propertychange', function () {
        let val = $(this).val();
        if (val.length > 0) {
            $('#search-hd').find('.pholder').hide(0);
        }
        else {
            // language=JQuery-CSS
            let index;
            index = $('#search-bd').find('li.selected').index();
            $('#search-hd').find('.pholder').eq(index).show().siblings('.pholder').hide(0);
        }
    });
    $('#search-bd').find('li').click(function () {
        let index = $(this).index();
        let search_hd = $('#search-hd');
        search_hd.find('.pholder').eq(index).show().siblings('.pholder').hide(0);
        search_hd.find('.search-hover' + 'tree-input').eq(index).show().siblings('.search-hovertree-input').hide(0);
        $(this).addClass('selected').siblings().removeClass('selected');
        search_hd.find('.search-hovertree-input').val('');
    });
});

$(function () {
    //js代码
    // language=JQuery-CSS
    $('#myCarousel').carousel({
        interval: 2000
    });
});

function mouseOver(obj) {
    let index;
    let imgArr = obj.getElementsByTagName("img");
    for (index = 0; index < imgArr.length; index++) {
        imgArr[index].style.width = "100px";
    }
    for (index = 0; index < imgArr.length; index++) {
        imgArr[index].style.height = "100px";
    }
}

function mouseOff(obj) {
    let index;
    let imgArr = obj.getElementsByTagName("img");
    for (index = 0; index < imgArr.length; index++) {
        imgArr[index].style.width = "90px";
    }
    for (index = 0; index < imgArr.length; index++) {
        imgArr[index].style.height = "90px";
    }
}
</script>
<!--引入bootstrap.min.js文件-->
<script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>