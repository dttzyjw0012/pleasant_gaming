package com.comfortable_game.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author success
 * <p>
 * 1. 属性
 * 2. 空参构造
 * 3. getter/setter
 * 4. toString
 */
public class Game implements Serializable {

    private static final long serialVersionUID = -800967679587302177L;

    //实体类中有什么?代码结构是什么?
    //属性[对象拥有"什么" - 页面中展示的该对象的信息] - 根据 表的列..
    private long id;

    private String gameName;

    private int gameTypeId;

    private long transactionNumber;

    Date releaseTime;

    private int operatingStatus;

    //多个游戏对应一个类别.
    //private GameType foodType;

    //alt + /
    public Game() {

    }

    /**
     * getter/setter方法
     **/
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public int getGameTypeId() {
        return gameTypeId;
    }

    public void setGameTypeId(int gameTypeId) {
        this.gameTypeId = gameTypeId;
    }

    public long getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(long transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Date getReleaseTime() {
        return releaseTime;
    }

    //	java.util.Date doem = new java.sql.Date();
    public void setReleaseTime(Date releaseTime) {
        this.releaseTime = releaseTime;
    }

    public int getOperatingStatus() {
        return operatingStatus;
    }

    public void setOperatingStatus(int operatingStatus) {
        this.operatingStatus = operatingStatus;
    }

    /**
     * toString
     **/
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Game [id = ");
        builder.append(id);
        builder.append(", gameName = ");
        builder.append(gameName);
        builder.append(", gameTypeId = ");
        builder.append(gameTypeId);
        builder.append(", transactionNumber = ");
        builder.append(transactionNumber);
        builder.append(", releaseTime = ");
        builder.append(releaseTime);
        builder.append(", operatingStatus = ");
        builder.append(operatingStatus);
        builder.append("]");
        return builder.toString();
    }
}
