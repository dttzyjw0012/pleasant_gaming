package com.comfortable_game.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 分类的对象
 *
 * @author Administrator
 */
public class GameType implements Serializable {

    private static final long serialVersionUID = 8842657199757395528L;

    private int id;

    private int gameType;

    private List<Game> games;//不要出现在toString方法体中

    public GameType() {
        // TODO Auto-generated constructor stub
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGameType() {
        return gameType;
    }

    public void setGameType(int gameType) {
        this.gameType = gameType;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GameType [id = ");
        builder.append(id);
        builder.append(", gameType = ");
        builder.append(gameType);
        builder.append("]");
        return builder.toString();
    }
}
