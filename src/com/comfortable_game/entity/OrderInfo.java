package com.comfortable_game.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author success
 * <p>
 * 1. 属性
 * 2. 空参构造
 * 3. getter/setter
 * 4. toString
 */
public class OrderInfo implements Serializable {

    private static final long serialVersionUID = -800967679587302177L;

    //实体类中有什么?代码结构是什么?
    //属性[对象拥有"什么" - 页面中展示的该对象的信息] - 根据 表的列..
    private long id;

    private long gameNameId;

    private String gameServerName;

    private int tradeTypeId;

    private long sellerId;

    private long buyerId;

    private double price;

    private Date releaaseTime;

    private Date orderTime;

    private Date completeTime;

    private int tradeStateId;

    private String tradeName;

    public void setId(long id) {
        this.id = id;
    }

    public void setGameNameId(long gameNameId) {
        this.gameNameId = gameNameId;
    }

    public void setGameServerName(String gameServerName) {
        this.gameServerName = gameServerName;
    }

    public void setTradeTypeId(int tradeTypeId) {
        this.tradeTypeId = tradeTypeId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setReleaaseTime(Date releaaseTime) {
        this.releaaseTime = releaaseTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    public void setTradeStateId(int tradeStateId) {
        this.tradeStateId = tradeStateId;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public void setInventoryQuantity(int inventoryQuantity) {
        this.inventoryQuantity = inventoryQuantity;
    }

    private int inventoryQuantity;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public long getGameNameId() {
        return gameNameId;
    }

    public String getGameServerName() {
        return gameServerName;
    }

    public int getTradeTypeId() {
        return tradeTypeId;
    }

    public long getSellerId() {
        return sellerId;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public double getPrice() {
        return price;
    }

    public Date getReleaaseTime() {
        return releaaseTime;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public int getTradeStateId() {
        return tradeStateId;
    }

    public String getTradeName() {
        return tradeName;
    }

    public int getInventoryQuantity() {
        return inventoryQuantity;
    }

    //alt + /
    public OrderInfo() {

    }

    /**getter/setter方法**/

    /**
     * toString
     **/
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Order [id = ");
        builder.append(id);
        builder.append(", gameNameId = ");
        builder.append(gameNameId);
        builder.append(", gameServerName = ");
        builder.append(gameServerName);
        builder.append(", tradeTypeId = ");
        builder.append(tradeTypeId);
        builder.append(", sellerId = ");
        builder.append(sellerId);
        builder.append(", buyerId = ");
        builder.append(buyerId);
        builder.append(", price = ");
        builder.append(price);
        builder.append(", releaaseTime = ");
        builder.append(releaaseTime);
        builder.append(", orderTime = ");
        builder.append(orderTime);
        builder.append(", completeTime = ");
        builder.append(completeTime);
        builder.append(", tradeStateId = ");
        builder.append(tradeStateId);
        builder.append(", tradeName = ");
        builder.append(tradeName);
        builder.append("]");
        return builder.toString();
    }
}
