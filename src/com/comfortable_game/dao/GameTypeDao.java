package com.comfortable_game.dao;

import com.comfortable_game.entity.GameType;

import java.util.List;

public interface GameTypeDao {
    /**
     * 类型
     *
     * @return
     */
    public List<GameType> findAll();

}
