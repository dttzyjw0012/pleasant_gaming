package com.comfortable_game.dao;

import com.comfortable_game.entity.Game;

import java.util.List;

/**
 * 接口设计
 *
 * @author Administrator
 */
public interface GameDao {
    /**
     * 根据id进行查询
     *
     * @param id
     * @return
     */
    public List<Game> allGameByHot();

    /**
     * 根据类型进行查询
     *
     * @param typeId 类型的id
     * @return
     */
    public List<Game> findByGameType(int typeId);
}