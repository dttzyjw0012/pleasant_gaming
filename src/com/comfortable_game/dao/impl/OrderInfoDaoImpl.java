package com.comfortable_game.dao.impl;

import com.comfortable_game.dao.GameDao;
import com.comfortable_game.dao.OrderInfoDao;
import com.comfortable_game.entity.Game;
import com.comfortable_game.entity.OrderInfo;
import com.comfortable_game.util.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 通过implements关键字来实现某个接口
 **/
public class OrderInfoDaoImpl implements OrderInfoDao {
    @Override
    public List<OrderInfo> searchOrder(String keywords) {
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

        //创建一个存放游戏对象的集合
        List<OrderInfo> orderInfos = new ArrayList<>();

        //1.获取连接
        try {
            conn = JdbcUtils.getConnection();

            //2.创建预编译语句对象
            String sql = "SELECT * FROM order_info WHERE INSTR(TRADE_NAME, ?) > 0 ORDER BY id";
            pst = conn.prepareStatement(sql);
            //发送参数 - 注意类型
            pst.setString(1, keywords);

            //3.执行sql
            rs = pst.executeQuery();

            //4.处理结果集
            while (rs.next()) {//每一行的数据代表一个具体的对象
                // int i = 10 - 只能存放单个值
                //int[] arr = {2,3,4,5};//可以存放多个值,缺陷 - 这些值只能是同一个类型的

                //创建实体类对象 - 想想成一个容器[可以存放任意类型任意多个的容器]
                OrderInfo orderInfo = new OrderInfo();
                orderInfo.setId(rs.getLong(1));
                orderInfo.setGameNameId(rs.getLong(2));
                orderInfo.setGameServerName(rs.getString(3));
                orderInfo.setTradeTypeId(rs.getInt(4));
                orderInfo.setSellerId(rs.getLong(5));
                orderInfo.setBuyerId(rs.getLong(6));
                orderInfo.setPrice(rs.getDouble(7));
                orderInfo.setReleaaseTime((Date) rs.getObject(8));
                orderInfo.setOrderTime((Date) rs.getObject(9));
                orderInfo.setCompleteTime((Date) rs.getObject(10));
                orderInfo.setTradeStateId(rs.getInt(11));
                orderInfo.setTradeName(rs.getString(12));
                orderInfo.setInventoryQuantity(rs.getInt(13));


                //将当前第一行得到的对象放入到集合中
                orderInfos.add(orderInfo);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn, pst, rs);
        }

        return orderInfos;//该方法执行完之后返回的结果就是一个集合
    }

    @Override
    public List<OrderInfo> searchOrder(String keywords, String orderBy) {
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

        //创建一个存放游戏对象的集合
        List<OrderInfo> orderInfos = new ArrayList<>();

        //1.获取连接
        try {
            conn = JdbcUtils.getConnection();

            //2.创建预编译语句对象
            String sql = "SELECT * FROM order_info WHERE INSTR(TRADE_NAME, ?) > 1 ORDER BY ?";
            pst = conn.prepareStatement(sql);
            //发送参数 - 注意类型
            pst.setString(1, keywords);
            pst.setString(2, orderBy);

            //3.执行sql
            rs = pst.executeQuery();

            //4.处理结果集
            while (rs.next()) {//每一行的数据代表一个具体的对象
                // int i = 10 - 只能存放单个值
                //int[] arr = {2,3,4,5};//可以存放多个值,缺陷 - 这些值只能是同一个类型的

                //创建实体类对象 - 想想成一个容器[可以存放任意类型任意多个的容器]
                OrderInfo orderInfo = new OrderInfo();
                orderInfo.setId(rs.getLong(1));
                orderInfo.setGameNameId(rs.getLong(2));
                orderInfo.setGameServerName(rs.getString(3));
                orderInfo.setTradeTypeId(rs.getInt(4));
                orderInfo.setSellerId(rs.getLong(5));
                orderInfo.setBuyerId(rs.getLong(6));
                orderInfo.setPrice(rs.getDouble(7));
                orderInfo.setReleaaseTime((Date) rs.getObject(8));
                orderInfo.setOrderTime((Date) rs.getObject(9));
                orderInfo.setCompleteTime((Date) rs.getObject(10));
                orderInfo.setTradeStateId(rs.getInt(11));
                orderInfo.setTradeName(rs.getString(12));
                orderInfo.setInventoryQuantity(rs.getInt(13));


                //将当前第一行得到的对象放入到集合中
                orderInfos.add(orderInfo);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn, pst, rs);
        }

        return orderInfos;//该方法执行完之后返回的结果就是一个集合
    }
}