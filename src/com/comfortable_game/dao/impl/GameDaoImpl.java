package com.comfortable_game.dao.impl;

import com.comfortable_game.dao.GameDao;
import com.comfortable_game.entity.Game;
import com.comfortable_game.util.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 通过implements关键字来实现某个接口
 **/
public class GameDaoImpl implements GameDao {

    @Override
    public List<Game> allGameByHot() {
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

        //创建一个存放游戏对象的集合
        List<Game> games = new ArrayList<>();

        //1.获取连接
        try {
            conn = JdbcUtils.getConnection();

            //2.创建预编译语句对象
            String sql = "select * from games ORDER BY transaction_number DESC";
            pst = conn.prepareStatement(sql);
            //发送参数 - 注意类型

            //3.执行sql
            rs = pst.executeQuery();

            //4.处理结果集
            while (rs.next()) {//每一行的数据代表一个具体的对象
                // int i = 10 - 只能存放单个值
                //int[] arr = {2,3,4,5};//可以存放多个值,缺陷 - 这些值只能是同一个类型的

                //创建实体类对象 - 想想成一个容器[可以存放任意类型任意多个的容器]
                Game game = new Game();
                game.setId(rs.getLong(1));
                game.setGameName(rs.getString(2));
                game.setGameTypeId(rs.getInt(3));
                game.setTransactionNumber(rs.getLong(4));
                game.setReleaseTime((Date) rs.getObject(5));
                game.setOperatingStatus(rs.getInt(6));

                //将当前第一行得到的对象放入到集合中
                games.add(game);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn, pst, rs);
        }

        return games;//该方法执行完之后返回的结果就是一个集合
    }

    @Override
    public List<Game> findByGameType(int typeId) {
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

        //创建一个存放游戏对象的集合
        List<Game> games = new ArrayList<>();

        //1.获取连接
        try {
            conn = JdbcUtils.getConnection();

            //2.创建预编译语句对象
            String sql = "select * from games WHERE game_type_id = ? ORDER BY id";
            pst = conn.prepareStatement(sql);
            //发送参数 - 注意类型
            pst.setInt(1, typeId);

            //3.执行sql
            rs = pst.executeQuery();

            //4.处理结果集
            while (rs.next()) {//每一行的数据代表一个具体的对象
                // int i = 10 - 只能存放单个值
                //int[] arr = {2,3,4,5};//可以存放多个值,缺陷 - 这些值只能是同一个类型的

                //创建实体类对象 - 想想成一个容器[可以存放任意类型任意多个的容器]
                Game game = new Game();
                game.setId(rs.getLong(1));
                game.setGameName(rs.getString(2));
                game.setGameTypeId(rs.getInt(3));
                game.setTransactionNumber(rs.getLong(4));
                game.setReleaseTime((Date) rs.getObject(5));
                game.setOperatingStatus(rs.getInt(6));

                //将当前第一行得到的对象放入到集合中
                games.add(game);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn, pst, rs);
        }

        return games;//该方法执行完之后返回的结果就是一个集合
    }
}