package com.comfortable_game.dao.impl;

import com.comfortable_game.dao.GameTypeDao;
import com.comfortable_game.entity.GameType;
import com.comfortable_game.util.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GameTypeDaoImpl implements GameTypeDao {

    @Override
    public List<GameType> findAll() {

        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;

        //创建一个存放游戏对象的集合
        List<GameType> games = new ArrayList<>();

        //1.获取连接
        try {
            conn = JdbcUtils.getConnection();

            //2.创建预编译语句对象
            String sql = "select * from game_types ORDER BY id";
            pst = conn.prepareStatement(sql);

            //3.执行sql
            rs = pst.executeQuery();

            //4.处理结果集
            while (rs.next()) {//每一行的数据代表一个具体的对象
                //通过列的序号进行获取...
                int id = rs.getInt(1);
                int game_type = rs.getInt(2);

                //创建实体类对象 - 想想成一个容器[可以存放任意类型任意多个的容器]
                GameType type = new GameType();
                type.setId(id);
                type.setGameType(game_type);

                //将当前第一行得到的对象放入到集合中
                games.add(type);
            }


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn, pst, rs);
        }
        return games;//该方法执行完之后返回的结果就是一个集合
    }

}
