package com.comfortable_game.dao;

import com.comfortable_game.entity.Game;
import com.comfortable_game.entity.OrderInfo;

import java.util.List;

/**
 * 接口设计
 *
 * @author Administrator
 */
public interface OrderInfoDao {
    /**
     * 根据id进行查询
     *
     * @param id
     * @return
     */
    public List<OrderInfo> searchOrder(String keywords);

    public List<OrderInfo> searchOrder(String keywords, String orderBy);
}