package com.comfortable_game.controller;

import com.comfortable_game.dao.impl.OrderInfoDaoImpl;
import com.comfortable_game.entity.OrderInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/SearchForm")
public class SearchAction extends HttpServlet {
    OrderInfoDaoImpl orderInfoDaoImpl = new OrderInfoDaoImpl();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchAction() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置响应内容类型
        Integer page = new Integer(1);

        String gameType = new String(req.getParameter("gameType").getBytes("UTF-8"));
        String tradeType = new String(req.getParameter("tradeType").getBytes("UTF-8"));
        page = new Integer(new String(req.getParameter("page")));
        String searchKey = new String(req.getParameter("searchKey"));

        List<OrderInfo> OrderInfosBySearch = orderInfoDaoImpl.searchOrder(searchKey, "id");

        req.setAttribute("searchKey", searchKey);
        req.setAttribute("gameType", gameType);
        req.setAttribute("tradeType", tradeType);
        req.setAttribute("orderInfosBySearch", OrderInfosBySearch);
        req.setAttribute("searchPage", page);

        req.getRequestDispatcher("/search.jsp").forward(req, resp);
    }
}
