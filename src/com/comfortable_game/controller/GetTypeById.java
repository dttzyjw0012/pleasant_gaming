package com.comfortable_game.controller;

public class GetTypeById {
    public String getTypeById(int typeId) {
        String typeName = "";

        switch (typeId) {
            case 0:
                typeName = "网络游戏";
                break;
            case 1:
                typeName = "手机游戏";
                break;
            case 2:
                typeName = "网页游戏";
                break;
            case 3:
                typeName = "棋牌游戏";
                break;
            default:
                typeName = "充值&其它";
                break;
        }
        return typeName;
    }
}