package com.comfortable_game.controller;

import com.comfortable_game.dao.GameDao;
import com.comfortable_game.dao.GameTypeDao;
import com.comfortable_game.dao.impl.GameDaoImpl;
import com.comfortable_game.dao.impl.GameTypeDaoImpl;
import com.comfortable_game.entity.Game;
import com.comfortable_game.entity.GameType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "")
public class IndexAction extends HttpServlet {

    private static final long serialVersionUID = 1435440518772299379L;

    //创建游戏类型的实现类对象
    private GameTypeDao gameTypeDao = new GameTypeDaoImpl();
    private GameDao gameDao = new GameDaoImpl();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //业务逻辑操作...
        List<GameType> gameTypelist = gameTypeDao.findAll();
        List<Game> gameListByHot = gameDao.allGameByHot();
        List<Game> gameListTypeZero = gameDao.findByGameType(0);

        GetTypeById getTypeById = new GetTypeById();

        //为了在jsp页面中能够获取到list,需要将它放入到作用域中
        req.setAttribute("gameTypelist", gameTypelist);
        req.setAttribute("gameListHot", gameListByHot);
        req.setAttribute("getTypeById", getTypeById);
        req.setAttribute("gameListTypeZero", gameListTypeZero);

        //指定它转向的页面.
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}